﻿using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;

public class UserModel
{
	public string UserId;
	public string Version;

	public ReactiveWrapper<int> LevelIndex = new ReactiveWrapper<int>();
	public ReactiveWrapper<int> SessionScore = new ReactiveWrapper<int>();
	public ReactiveWrapper<int> BestScore = new ReactiveWrapper<int>();

	public bool IsTutorial => LevelIndex.Value == 0;

	public static UserModel LoadUserModel()
	{
		var path = Application.persistentDataPath + "/usermodel.json";

		if (File.Exists(path))
		{
			try
			{
				var raw = File.ReadAllText(path);

				var dump = JsonConvert.DeserializeObject<UserDump>(raw);

				if (dump == null)
				{
					return GetFreshModel();
				}

				if (string.IsNullOrEmpty(dump.version))
				{
					return GetFreshModel();
				}

				if (!CheckVersion(dump.version))
				{
					return GetFreshModel();
				}

				return CreateUserModel(dump);
			}
			catch (Exception)
			{
				return GetFreshModel();
			}
		}

		return GetFreshModel();
	}

	public static void SaveUserModel(UserModel userModel)
	{
		var path = Application.persistentDataPath + "/usermodel.json";

		var dump = CreateUserDump(userModel);

		var raw = JsonConvert.SerializeObject(dump);

		File.WriteAllText(path, raw);
	}

	private static bool CheckVersion(string currentVersionRaw)
	{
		var requiredVersionRaw = Application.version;

		var currentVersion = new Version(currentVersionRaw);
		var requiredVersion = new Version(requiredVersionRaw);

		return currentVersion >= requiredVersion;
	}

	private static UserModel CreateUserModel(UserDump dump)
	{
		var model = new UserModel();

		model.UserId = dump.userId;
		model.Version = dump.version;
		model.LevelIndex.Value = dump.levelIndex;
		model.BestScore.Value = dump.bestScore;

		return model;
	}

	private static UserDump CreateUserDump(UserModel model)
	{
		var dump = new UserDump();

		dump.userId = model.UserId;
		dump.version = model.Version;
		dump.levelIndex = model.LevelIndex.Value;
		dump.bestScore = model.BestScore.Value;

		return dump;
	}

	private static UserModel GetFreshModel()
	{
		var model = new UserModel();

		model.UserId = Guid.NewGuid().ToString();
		model.Version = Application.version;
		model.LevelIndex.Value = 0;
		model.BestScore.Value = 0;

		return model;
	}

	public class UserDump
	{
		public string userId;
		public string version;
		public int levelIndex;
		public int bestScore;
	}

#if UNITY_EDITOR
	[UnityEditor.MenuItem("Game/Remove User Model")]
	private static void RemoveUserModel()
	{
		var path = Application.persistentDataPath + "/usermodel.json";

		if (File.Exists(path))
		{
			File.Delete(path);
		}

		UnityEngine.Debug.Log("User Model removed.");
	}
#endif
}