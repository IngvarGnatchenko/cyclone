﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class GameAssembler : MonoBehaviour
{
    [SerializeField] private AppStateWatcher appStateWatcher;
    [SerializeField] private GameContent gameContent;
    [SerializeField] private ShipView shipView;
    [SerializeField] private TrailView trailView;
    [SerializeField] private EnvironmentView environmentView;
    [SerializeField] private ShipController shipController;
    [SerializeField] private CannonAssembler cannonAssembler;
    [SerializeField] private MineAssembler mineAssembler;
    [SerializeField] private CameraShaker cameraShaker;
    [SerializeField] private Camera activeCamera;
    [SerializeField] private RectTransform activeCanvas;
    [SerializeField] private GameEffectController gameEffectController;
    [SerializeField] private HolesView holesView;
    [SerializeField] private Button cheatButton;

    [SerializeField] private TutorialGameScreen tutorialGameScreen;
    [SerializeField] private StartGameScreen startGameScreen;
    [SerializeField] private RunningGameScreen runningGameScreen;
    [SerializeField] private VictoryGameScreen victoryGameScreen;
    [SerializeField] private DefeatGameScreen defeatGameScreen;
    [SerializeField] private UnlockGameScreen unlockGameScreen;

    private const int TargetFrameRate = 60;

    private TimeSystem timeSystem;
    private GameModel gameModel;
    private LevelGenerator levelGenerator;
    private GameCycleController gameCycleController;
    
    private NodeGenerator nodeGenerator;
    private IntersectionController intersectionController;
    private EnvironmentController environmentController;
    private HolesController holesController;
    private UIWindowController uiWindowController;

    private UserModel userModel;

    private bool isInitialStart = true;

    private void Awake()
    {
        Application.targetFrameRate = TargetFrameRate;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        GameContent.Instance = gameContent;

        userModel = UserModel.LoadUserModel();

        levelGenerator = new LevelGenerator();

        timeSystem = new TimeSystem();

        uiWindowController = new UIWindowController(new List<UIWindow>()
        {
            tutorialGameScreen,
            startGameScreen,
            runningGameScreen,
            victoryGameScreen,
            defeatGameScreen,
            unlockGameScreen
        });

        appStateWatcher.ApplicationPause += OnApplicationPaused;
        cheatButton.onClick.AddListener(() => OnGameFinished(true));

        Init();
    }

    private void OnDestroy()
    {
        appStateWatcher.ApplicationPause -= OnApplicationPaused;
        cheatButton.onClick.RemoveAllListeners();

        Deinit();
    }

    private async void Init()
    {
        var levelContent = GameContent.Instance.GetLevelContentByIndex(userModel.LevelIndex.Value);
        var fuels = levelGenerator.GenerateFuels(levelContent);
        var cannons = levelGenerator.GenerateCannons(levelContent);
        var holes = levelGenerator.GenerateHoles(levelContent);
        var mines = levelGenerator.GenerateMines(levelContent);
        var walls = levelGenerator.GenerateWalls(levelContent);

        var shipModel = new ShipModel(levelContent.ShipContent);
        gameModel = new GameModel(shipModel, new EnvironmentModel(fuels, walls, levelContent.Background), cannons, holes, mines);

        gameCycleController = new GameCycleController(gameModel, userModel, gameModel.Level, shipController);
        nodeGenerator = new NodeGenerator(gameModel);
        intersectionController = new IntersectionController(gameModel.Trail);
        environmentController = new EnvironmentController(gameModel.Level, intersectionController);
        holesController = new HolesController(gameModel.Holes, gameModel.Ship);

        gameCycleController.GameFinished += OnGameFinished;
        gameModel.GameState.Changed += OnGameStateChanged;

        shipController.Init(gameModel.Ship, gameModel.Holes, timeSystem, UIUtils.GetScreenBorder(activeCamera), userModel.IsTutorial);
        intersectionController.Init();
        environmentController.Init();
        holesController.Init();
        gameCycleController.Init();
        gameEffectController.Init(gameModel, gameModel.Level, gameModel.Cannons, gameModel.Mines, cameraShaker, activeCamera, activeCanvas);
        shipView.Init(gameModel.Ship);
        trailView.Init(gameModel.Trail);
        environmentView.Init(gameModel.Level);
        holesView.Init(gameModel.Holes);
        nodeGenerator.Init();
        cannonAssembler.Init(gameModel.Cannons, gameModel.Ship);
        mineAssembler.Init(gameModel.Mines, gameModel.Ship);

        tutorialGameScreen.Init();
        startGameScreen.Init();
        runningGameScreen.Init(gameModel, userModel);
        victoryGameScreen.Init();
        unlockGameScreen.Init();
        defeatGameScreen.Init();

        await StartGame();

        gameModel.GameState.Value = GameState.Running;
    }

    private async Task StartGame()
    {
        if (userModel.IsTutorial)
        {
            gameModel.GameState.Value = GameState.Tutorial;

            await tutorialGameScreen.Wait();
        }
        else
        {
            gameModel.GameState.Value = GameState.Start;

            if (isInitialStart)
            {
                await startGameScreen.Wait(userModel.BestScore.Value);
            }
        }

        isInitialStart = false;
    }

    private void Deinit()
    {
        gameCycleController.GameFinished -= OnGameFinished;
        gameModel.GameState.Changed -= OnGameStateChanged;

        shipController.Deinit();
        intersectionController.Deinit();
        environmentController.Deinit();
        holesController.Deinit();
        gameCycleController.Deinit();
        gameEffectController.Deinit();
        shipView.Deinit();
        trailView.Deinit();
        environmentView.Deinit();
        holesView.Deinit();
        nodeGenerator.Deinit();
        cannonAssembler.Deinit();
        mineAssembler.Deinit();

        tutorialGameScreen.Deinit();
        startGameScreen.Deinit();
        runningGameScreen.Deinit();
        victoryGameScreen.Deinit();
        unlockGameScreen.Deinit();
        defeatGameScreen.Deinit();
    }

    private void OnApplicationPaused(bool pause)
    {
        UserModel.SaveUserModel(userModel);
    }

    private void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Tutorial:
                uiWindowController.Show(UIWindowId.TutorialWindow);
                break;
            case GameState.Start:
                if (isInitialStart)
                {
                    uiWindowController.Show(UIWindowId.StartWindow);
                }
                break;
            case GameState.Running:
                uiWindowController.Show(UIWindowId.RunningWindow);
                break;
            case GameState.Victory:
                uiWindowController.Show(UIWindowId.VictoryWindow);
                break;
            case GameState.Defeat:
                uiWindowController.Show(UIWindowId.DefeatWindow);
                break;
        }
    }

    private void Update()
    {
        if (gameModel.GameState.Value == GameState.Running)
        {
            nodeGenerator.Update();
            environmentController.Update();
            cannonAssembler.ExternalUpdate();
            mineAssembler.ExternalUpdate();
            shipController.ExternalUpdate();
            runningGameScreen.ExternalUpdate();
        }

        timeSystem.Update();
        cameraShaker.ExternalUpdate();
        gameEffectController.ExternalUpdate();
    }

    private async void OnGameFinished(bool isVictory)
    {
        gameModel.GameState.Value = isVictory ? GameState.Victory : GameState.Defeat;

        userModel.BestScore.Value = Mathf.Max(userModel.BestScore.Value, userModel.SessionScore.Value);

        if (isVictory)
        {
            var userLevel = userModel.LevelIndex.Value;
            var userScore = userModel.SessionScore.Value;

            userModel.LevelIndex.Value += 1;

            await victoryGameScreen.Wait(userLevel, userScore);

            await HandleUnlockScreen(userModel.LevelIndex.Value);
        }
        else
        {
            var userLevel = userModel.LevelIndex.Value;
            var userScore = userModel.SessionScore.Value;

            userModel.SessionScore.Value = 0;

            await defeatGameScreen.Wait(userLevel, userScore);
        }

        Deinit();

        Init();
    }

    private async Task HandleUnlockScreen(int levelIndex)
    {
        var levelContent = GameContent.Instance.GetLevelContentByIndex(levelIndex);

        if (levelContent.IsShipUnlocked)
        {
            uiWindowController.Show(UIWindowId.UnlockWindow);

            await unlockGameScreen.Show(levelContent.ShipContent);
        }
    }
}
