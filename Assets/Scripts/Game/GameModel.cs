﻿using System.Collections.Generic;

public class GameModel
{
    public GameModel(
        ShipModel ship,
        EnvironmentModel environmentModel, 
        List<CannonModel> cannons, 
        List<HoleModel> holes, 
        List<MineModel> mines)
    {
        Ship = ship;
        Level = environmentModel;
        Cannons = cannons;
        Holes = holes;
        Mines = mines;
        WinScore = GetWinScore();
    }

    public int WinScore { get; }

    public ReactiveWrapper<GameState> GameState = new ReactiveWrapper<GameState>();

    public EnvironmentModel Level { get; }

    public List<CannonModel> Cannons { get; } = new List<CannonModel>();

    public List<HoleModel> Holes { get; } = new List<HoleModel>();

    public List<MineModel> Mines { get; } = new List<MineModel>();

    public ShipModel Ship { get; }

    public TrailModel Trail { get; } = new TrailModel();

    public ReactiveWrapper<int> Score { get; } = new ReactiveWrapper<int>();

    private int GetWinScore()
    {
        var runningScore = 0;

        Level.Fuels.ForEach(fuel =>
        {
            if (fuel.Content.Type == FuelType.Normal)
            {
                runningScore += fuel.Content.Score;
            }
        });

        return runningScore;
    }
}

public enum GameState
{
    Tutorial,
    Start,
    Running,
    Victory,
    Defeat
}
