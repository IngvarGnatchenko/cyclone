﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameEffectController : MonoBehaviour
{
    [SerializeField] private UIFloodFuelsView floodView;
    [SerializeField] private Transform floodTarget;
    [SerializeField] private Transform shipEffect;
    [SerializeField] private Transform missileEffect;
    [SerializeField] private Transform fuelEffect;
    [SerializeField] private Transform shellEffect;
    [SerializeField] private Transform mineEffect;
    [SerializeField] private AudioSource audioFuelTaken;
    [SerializeField] private AudioSource audioBoostTaken;
    [SerializeField] private AudioSource audioArmorTaken;
    [SerializeField] private AudioSource audioMineActivated;
    [SerializeField] private AudioSource audioMissileFire;
    [SerializeField] private AudioSource audioMissileExpire;
    [SerializeField] private AudioSource audioShellFire;
    [SerializeField] private AudioSource audioShellExpire;

    private const float FlyTime = 1.0f;
    private const float TimeToLive = 3f;

    private GameModel gameModel;
    private EnvironmentModel environmentModel;
    private List<CannonModel> cannonModels;
    private List<MineModel> mineModels;
    private CameraShaker cameraShaker;
    private Camera activeCamera;
    private RectTransform activeCanvas;

    public void Init(GameModel gameModel, EnvironmentModel environmentModel, List<CannonModel> cannonModels, List<MineModel> mineModels, CameraShaker cameraShaker, Camera activeCamera, RectTransform activeCanvas)
    {
        this.gameModel = gameModel;
        this.environmentModel = environmentModel;
        this.cannonModels = cannonModels;
        this.mineModels = mineModels;
        this.cameraShaker = cameraShaker;
        this.activeCamera = activeCamera;
        this.activeCanvas = activeCanvas;

        floodView.Init();

        gameModel.Ship.Collided += OnShipCollided;
        gameModel.Ship.Detonated += OnShipDetonated;
        environmentModel.FuelTaken += OnFuelTaken;

        cannonModels.ForEach(model =>
        {
            model.MissileAdded += OnMissileAdded;
            model.MissileDetonated += OnMissileDetonated;
        });
        mineModels.ForEach(model =>
        {
            model.ShellAdded += OnShellAdded;
            model.ShellDetonated += OnShellDetonated;
            model.Activated.Changed += OnMineActivatedChanged;
            model.Detonated += OnMineDetonated;
        });
    }

    public void Deinit()
    {
        floodView.Deinit();

        gameModel.Ship.Collided -= OnShipCollided;
        gameModel.Ship.Detonated -= OnShipDetonated;
        environmentModel.FuelTaken -= OnFuelTaken;

        cannonModels.ForEach(model =>
        {
            model.MissileAdded -= OnMissileAdded;
            model.MissileDetonated -= OnMissileDetonated;
        });
        mineModels.ForEach(model =>
        {
            model.ShellDetonated -= OnShellDetonated;
            model.ShellAdded -= OnShellAdded;
            model.Activated.Changed -= OnMineActivatedChanged;
            model.Detonated -= OnMineDetonated;
        });
    }

    public void ExternalUpdate()
    {
        floodView.UpdateEffectors();
    }

    private void OnShellAdded(ShellModel model)
    {
        // Nothing here...
    }

    private void OnShellDetonated(ShellModel model)
    {
        audioShellExpire.Play();

        CreateEffect(shellEffect, model.Position.Value, TimeToLive);
    }

    private void OnMineDetonated(MineModel model)
    {
        audioShellFire.Play();

        CreateEffect(mineEffect, model.Content.Position, TimeToLive);
    }

    private void OnMineActivatedChanged(bool isActivated)
    {
        if (isActivated)
        {
            audioMineActivated.Play();
        }
    }

    private void OnFuelTaken(FuelModel fuelModel)
    {
        cameraShaker.Shake();

        if (fuelModel.Content.Type == FuelType.Normal)
        {
            var canvasLocalPosition = UIUtils.FromWorldToCanvasLocal(activeCanvas, activeCamera, fuelModel.Content.Position);

            floodView.Effect(fuelModel, canvasLocalPosition, floodTarget.localPosition, FlyTime);
        }
        
        CreateEffect(fuelEffect, fuelModel.Content.Position, TimeToLive);

        PlayEffectSound(fuelModel.Content);
    }

    private void PlayEffectSound(FuelContent content)
    {
        if (content.Type == FuelType.Normal)
        {
            audioFuelTaken.Play();
        }
        else if (content.Type == FuelType.Boost)
        {
            audioBoostTaken.Play();
        }
        else if (content.Type == FuelType.Armor)
        {
            audioArmorTaken.Play();
        }
    }

    private void OnShipCollided(WallView wallView)
    {
        cameraShaker.Shake();

        CreateEffect(shipEffect, gameModel.Ship.Position.Value, TimeToLive);
    }

    private void OnShipDetonated()
    {
        cameraShaker.Shake();

        CreateEffect(shipEffect, gameModel.Ship.Position.Value, TimeToLive);
    }

    private void OnMissileAdded(MissileModel model)
    {
        audioMissileFire.Play();
    }

    private void OnMissileDetonated(MissileModel missile)
    {
        audioMissileExpire.Play();

        CreateEffect(missileEffect, missile.Position.Value, TimeToLive);
    }

    private void CreateEffect(Transform prefab, Vector3 position, float timeToLive)
    {
        var instance = GameObject.Instantiate(prefab, position, Quaternion.identity);

        GameObject.Destroy(instance.gameObject, timeToLive);
    }
}
