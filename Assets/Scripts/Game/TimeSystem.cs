﻿using UnityEngine;

public class TimeSystem
{
    public ReactiveWrapper<float> CurrentTime = new ReactiveWrapper<float>();

    public TimeSystem()
    {
        CurrentTime.Value = 1000;
    }

    public void Update()
    {
        CurrentTime.Value += Time.deltaTime;
    }
}
