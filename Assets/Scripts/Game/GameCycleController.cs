﻿using System;

public class GameCycleController
{
    public event Action<bool> GameFinished;

    private GameModel gameModel;
    private UserModel userModel;
    private EnvironmentModel environmentModel;
    private ShipController shipController;

    private bool gameFinished;

    public GameCycleController(GameModel gameModel, UserModel userModel, EnvironmentModel environmentModel, ShipController shipController)
    {
        this.gameModel = gameModel;
        this.userModel = userModel;
        this.environmentModel = environmentModel;
        this.shipController = shipController;
    }

    public void Init()
    {
        gameModel.Score.Changed += OnScoreChanged;
        gameModel.Ship.Collided += OnShipCollided;
        gameModel.Ship.Detonated += OnShipDetonated;
        environmentModel.FuelTaken += OnFuelTaken;
    }

    public void Deinit()
    {
        gameModel.Score.Changed -= OnScoreChanged;
        gameModel.Ship.Collided -= OnShipCollided;
        gameModel.Ship.Detonated -= OnShipDetonated;
        environmentModel.FuelTaken -= OnFuelTaken;
    }

    private void OnFuelTaken(FuelModel fuelModel)
    {
        switch (fuelModel.Content.Type)
        {
            case FuelType.Normal:
                userModel.SessionScore.Value += fuelModel.Content.Score;
                gameModel.Score.Value += fuelModel.Content.Score;
                break;
            case FuelType.Boost:
                shipController.ActivateBoost();
                break;
            case FuelType.Armor:
                shipController.ActivateArmor();
                break;
        }
    }

    private void OnScoreChanged(int score)
    {
        if (score >= gameModel.WinScore && !gameFinished)
        {
            gameFinished = true;

            GameFinished?.Invoke(true);
        }
    }

    private void OnShipCollided(WallView WallView)
    {
        if (!gameFinished)
        {
            gameFinished = true;

            GameFinished?.Invoke(false);
        }
    }

    private void OnShipDetonated()
    {
        if (!gameFinished)
        {
            gameFinished = true;

            GameFinished?.Invoke(false);
        }
    }
}
