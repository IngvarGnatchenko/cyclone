﻿using UnityEngine;

public class ShellModel
{
    public int Id { get; }

    public bool IsExpired => timeToLive < 0.0f;

    public ReactiveWrapper<Vector2> Position = new ReactiveWrapper<Vector2>();
    public ReactiveWrapper<Vector2> Direction = new ReactiveWrapper<Vector2>();

    public Vector2 InitialDirection { get; }

    private float timeToLive;

    public ShellModel(int id, Vector2 position, Vector2 direction, float timeToLive)
    {
        Id = id;

        Position.Value = position;
        Direction.Value = direction;

        InitialDirection = direction;

        this.timeToLive = timeToLive;
    }

    public void Age()
    {
        timeToLive = timeToLive - Time.deltaTime;
    }
}
