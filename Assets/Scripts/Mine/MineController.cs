﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MineController
{
    private MineModel model;
    private ShipModel ship;

    private List<ShellModel> expiredTemp = new List<ShellModel>();
    private List<ShellModel> detonatedTemp = new List<ShellModel>();

    private const int ShellDirectionChangeSpeed = 4;
    private const float ShellDirectionDeviationAngle = 45.0f;
    private const float ShellDirectionOffset = 45.0f;

    private int idCounter;

    private CycledTimer activationTimer = new CycledTimer();

    public MineController(MineModel model, ShipModel ship)
    {
        this.model = model;
        this.ship = ship;
    }

    public void Init()
    {
        activationTimer.Ticked += OnActivationTicked;

        ship.Position.Changed += OnPositionChanged;
    }

    public void Deinit()
    {
        activationTimer.Ticked -= OnActivationTicked;

        activationTimer.Stop();

        ship.Position.Changed -= OnPositionChanged;
    }

    public void ExternalUpdate()
    {
        activationTimer.Update();
        
        ProcessShells();
    }

    private void ProcessShells()
    {
        expiredTemp.Clear();
        detonatedTemp.Clear();

        foreach (var shell in model.Shells)
        {
            ProcessPosition(shell);
            ProcessDirection(shell);

            shell.Age();

            if (shell.IsExpired)
            {
                expiredTemp.Add(shell);
            }
            else
            {
                if (IsShellDetonated(shell, ship))
                {
                    detonatedTemp.Add(shell);
                }
            }
        }

        foreach (var shell in expiredTemp)
        {
            model.Shells.Remove(shell);
            model.PublishShellDetonated(shell);
        }

        foreach (var shell in detonatedTemp)
        {
            model.Shells.Remove(shell);
            model.PublishShellDetonated(shell);
        }

        if (detonatedTemp.Count > 0)
        {
            if (!ship.Armor.Value)
            {
                ship.PublishDetonated();
            }
        }
    }

    private void ProcessPosition(ShellModel shell)
    {
        shell.Position.Value = shell.Position.Value + shell.Direction.Value * model.Content.ShellSpeed * Time.deltaTime;
    }

    private void ProcessDirection(ShellModel shell)
    {
        var functionalRotation = 
            (int)(Time.time * ShellDirectionChangeSpeed) % 2 == 0 
            ? Quaternion.Euler(0.0f, 0.0f, ShellDirectionDeviationAngle) 
            : Quaternion.Euler(0.0f, 0.0f, -ShellDirectionDeviationAngle);

        var targetDirection = functionalRotation * shell.InitialDirection;

        shell.Direction.Value = Vector3.RotateTowards(
            shell.Direction.Value,
            targetDirection,
            GameContent.Instance.ShellRotationSpeed * Time.deltaTime,
            0.0f);
    }

    private bool IsShellDetonated(ShellModel shell, ShipModel ship)
    {
        var detonationDistance = GameContent.Instance.ShellDetonationDistance;

        var distance = Vector2.Distance(shell.Position.Value, ship.Position.Value);

        return distance < detonationDistance;
    }

    private void OnPositionChanged(Vector2 position)
    {
        if (model.Activated.Value)
        {
            return;
        }

        if (CheckMineActvation(model, ship))
        {
            model.Activated.Value = true;

            var delay = GameContent.Instance.MineActivationDelayMs;

            activationTimer.Fire(delay);
        }
    }

    private void OnActivationTicked()
    {
        model.PublishMineDetonated();

        activationTimer.Stop();

        for (var i = 0; i < model.Content.ShellCount; i++)
        {
            var shellPosition = model.Content.Position;
            var shellDirection = GetCircleDirection(ShellDirectionOffset, i, model.Content.ShellCount);
            var shellTimeToLive = GameContent.Instance.ShellTimeToLive;

            var shell = new ShellModel(idCounter++, shellPosition, shellDirection, shellTimeToLive);

            model.Shells.Add(shell);
            model.PublishShellAdded(shell);
        }
    }

    private Vector2 GetCircleDirection(float offset, int shellIndex, int shellCount)
    {
        var step = (float)shellIndex / (float)shellCount;

        var angle = step * 360.0f + offset;

        return Quaternion.Euler(0.0f, 0.0f, angle) * Vector2.right;
    }

    private Vector2 GetRandomDirection()
    {
        return UnityEngine.Random.insideUnitCircle.normalized;
    }

    private bool CheckMineActvation(MineModel mine, ShipModel ship)
    {
        var minePosition = mine.Content.Position;
        var shipPosition = ship.Position.Value;

        var distance = Vector2.Distance(minePosition, shipPosition);

        return distance < mine.Content.ActivationRadius;
    }
}
