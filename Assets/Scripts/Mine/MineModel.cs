﻿using System;
using System.Collections.Generic;

public class MineModel
{
    public event Action<ShellModel> ShellAdded;
    public event Action<ShellModel> ShellDetonated;
    public event Action<MineModel> Detonated;

    public ReactiveWrapper<bool> Activated = new ReactiveWrapper<bool>();

    public List<ShellModel> Shells = new List<ShellModel>();

    public MineContent Content { get; }

    public MineModel(MineContent content)
    {
        Content = content;
    }

    public void PublishShellAdded(ShellModel shell)
    {
        ShellAdded?.Invoke(shell);
    }

    public void PublishShellDetonated(ShellModel shell)
    {
        ShellDetonated?.Invoke(shell);
    }

    public void PublishMineDetonated()
    {
        Detonated?.Invoke(this);
    }
}
