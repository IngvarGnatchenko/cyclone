﻿using System.Collections.Generic;
using UnityEngine;

public class MineAssembler : MonoBehaviour
{
    [SerializeField] private MineView viewPrefab;
    [SerializeField] private Transform viewAnchor;
    [SerializeField] private Transform shellAnchor;

    private List<MineView> views = new List<MineView>();
    private List<MineController> controllers = new List<MineController>();

    public void Init(List<MineModel> mines, ShipModel ship)
    {
        foreach (var mine in mines)
        {
            var view = GetView();
            var controller = new MineController(mine, ship);

            views.Add(view);
            controllers.Add(controller);

            view.Init(mine, shellAnchor);
            controller.Init();
        }
    }

    public void Deinit()
    {
        foreach (var controller in controllers)
        {
            controller.Deinit();
        }

        controllers.Clear();

        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    public void ExternalUpdate()
    {
        controllers.ForEach(item => item.ExternalUpdate());
    }

    private MineView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(MineView view)
    {
        GameObject.Destroy(view.gameObject);
    }
}
