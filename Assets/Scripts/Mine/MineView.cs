﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MineView : MonoBehaviour
{
    [SerializeField] private Transform anchor;
    [SerializeField] private Transform area;
    [SerializeField] private ShellView shelViewPrefab;
    [SerializeField] private SpriteRenderer activatedImage;
    [SerializeField] private SpriteRenderer deactivatedImage;

    public MineModel Model { get; private set; }

    private List<ShellView> views = new List<ShellView>();

    private Transform shellAnchor;

    public void Init(MineModel model, Transform shellAnchor)
    {
        Model = model;
        this.shellAnchor = shellAnchor;

        anchor.localPosition = model.Content.Position;
        area.localScale = Vector3.one * model.Content.ActivationRadius * 2.0f;

        model.Activated.Changed += OnActivatedChanged;
        model.ShellAdded += OnShellAdded;
        model.ShellDetonated += OnShellDetonated;
        model.Detonated += OnMineDetonated;

        OnActivatedChanged(model.Activated.Value);
    }

    public void Deinit()
    {
        Model.Activated.Changed -= OnActivatedChanged;
        Model.ShellAdded -= OnShellAdded;
        Model.ShellDetonated -= OnShellDetonated;
        Model.Detonated -= OnMineDetonated;

        DeinitShells();
    }

    private void OnShellAdded(ShellModel shell)
    {
        var view = GetView();

        view.Init(shell);

        views.Add(view);
    }

    private void OnShellDetonated(ShellModel shell)
    {
        var view = views.Find(item => item.Model.Id == shell.Id);

        view.Deinit();

        DestroyView(view);

        views.Remove(view);
    }

    private ShellView GetView()
    {
        return GameObject.Instantiate(shelViewPrefab, shellAnchor);
    }

    private void DestroyView(ShellView view)
    {
        GameObject.Destroy(view.gameObject);
    }

    private void DeinitShells()
    {
        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    private void OnActivatedChanged(bool isActivated)
    {
        activatedImage.gameObject.SetActive(isActivated);
        deactivatedImage.gameObject.SetActive(!isActivated);
    }

    private void OnMineDetonated(MineModel mine)
    {
        anchor.gameObject.SetActive(false);
    }
}
