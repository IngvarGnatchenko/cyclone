﻿using System;
using UnityEngine;

public class ShellView : MonoBehaviour
{
    [SerializeField] private Transform positionAnchor;
    [SerializeField] private Transform rotationAnchor;

    public ShellModel Model { get; private set; }

    public void Init(ShellModel model)
    {
        Model = model;

        model.Position.Changed += OnPositionChanged;
        model.Direction.Changed += OnDirectionChanged;

        OnPositionChanged(model.Position.Value);
    }

    public void Deinit()
    {
        Model.Position.Changed -= OnPositionChanged;
        Model.Direction.Changed -= OnDirectionChanged;
    }

    private void OnPositionChanged(Vector2 position)
    {
        positionAnchor.localPosition = position;
    }

    private void OnDirectionChanged(Vector2 direction)
    {
        rotationAnchor.localRotation = UIUtils.GetQuaternionFromVector(direction);
    }
}
