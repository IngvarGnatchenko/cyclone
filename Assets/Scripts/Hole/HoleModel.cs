﻿public class HoleModel
{
    public ReactiveWrapper<bool> Capturing = new ReactiveWrapper<bool>();

    public HoleContent Content { get; }

    public HoleModel(HoleContent content)
    {
        Content = content;
    }
}
