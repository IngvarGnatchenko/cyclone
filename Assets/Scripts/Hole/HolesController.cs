﻿using System.Collections.Generic;
using UnityEngine;

public class HolesController
{
    private List<HoleModel> holes;
    private ShipModel ship;

    public HolesController(List<HoleModel> holes, ShipModel ship)
    {
        this.holes = holes;
        this.ship = ship;
    }

    public void Init()
    {
        ship.Position.Changed += OnPositionChanged;
    }

    public void Deinit()
    {
        ship.Position.Changed -= OnPositionChanged;
    }

    private void OnPositionChanged(Vector2 position)
    {
        foreach (var hole in holes)
        {
            var distance = Vector2.Distance(ship.Position.Value, hole.Content.Position);

            hole.Capturing.Value = distance < hole.Content.CaptureRadius;
        }
    }
}
