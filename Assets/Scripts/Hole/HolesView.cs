﻿using System.Collections.Generic;
using UnityEngine;

public class HolesView : MonoBehaviour
{
    [SerializeField] private HoleView viewPrefab;
    [SerializeField] private Transform viewAnchor;

    private List<HoleView> views = new List<HoleView>();

    public void Init(List<HoleModel> holes)
    {
        foreach (var hole in holes)
        {
            var view = GetView();

            view.Init(hole);

            views.Add(view);
        }
    }

    public void Deinit()
    {
        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    private HoleView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(HoleView view)
    {
        GameObject.Destroy(view.gameObject);
    }
}
