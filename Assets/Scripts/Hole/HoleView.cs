﻿using UnityEngine;

public class HoleView : MonoBehaviour
{
    [SerializeField] private Transform capturingOn;
    [SerializeField] private Transform capturingOff;
    [SerializeField] private Transform area;
    [SerializeField] private Transform anchor;

    private HoleModel model;

    public void Init(HoleModel model)
    {
        this.model = model;

        anchor.localPosition = model.Content.Position;
        area.localScale = Vector3.one * model.Content.CaptureRadius * 2.0f;

        model.Capturing.Changed += OnCapturingChanged;
    }

    public void Deinit()
    {
        model.Capturing.Changed -= OnCapturingChanged;
    }

    private void OnCapturingChanged(bool capturing)
    {
        capturingOn.gameObject.SetActive(capturing);
        capturingOff.gameObject.SetActive(!capturing);
    }
}
