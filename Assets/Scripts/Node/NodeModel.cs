﻿using UnityEngine;

public class NodeModel
{
    public int Id { get; }
    public Vector2 Position { get; }
    public NodeModel Parent { get; set; }
    public NodeModel Child { get; set; }

    public NodeModel(int id, Vector2 position)
    {
        Id = id;
        Position = position;
    }
}
