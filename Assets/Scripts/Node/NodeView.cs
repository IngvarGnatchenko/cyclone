﻿using UnityEngine;

public class NodeView : MonoBehaviour
{
    [SerializeField] private Transform anchor;

    public NodeModel Model { get; private set; }

    public void Init(NodeModel model)
    {
        Model = model;

        anchor.localPosition = model.Position;
    }

    public void Deinit()
    {
    }
}
