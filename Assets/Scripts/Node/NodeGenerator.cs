﻿public class NodeGenerator
{
    private CycledTimer timer = new CycledTimer();

    private GameModel model;

    private int idCounter;
    private int currentNodesCount;

    public NodeGenerator(GameModel model)
    {
        this.model = model;
    }

    public void Init()
    {
        idCounter = 0;
        currentNodesCount = 0;

        timer.Ticked += OnTicked;
        timer.Fire(GameContent.Instance.NodePeriodMs);

        var childNode = GenerateNode();

        model.Trail.HeadNode.Value = childNode;
        model.Trail.TailNode.Value = childNode;
    }

    public void Deinit()
    {
        timer.Ticked -= OnTicked;
    }

    public void Update()
    {
        timer.Update();
    }

    private void OnTicked()
    {
        var childNode = GenerateNode();

        model.Trail.HeadNode.Value = childNode;

        if (currentNodesCount >= GameContent.Instance.InitialNodesCount + model.Score.Value)
        {
            RemoveNode();
        }
    }

    private NodeModel GenerateNode()
    {
        currentNodesCount += 1;

        var id = GenerateNodeId();
        var currentPosition = model.Ship.Position.Value;
        var parentNode = model.Trail.HeadNode.Value;

        var childNode = new NodeModel(id, currentPosition);
        childNode.Parent = parentNode;

        if (parentNode != null)
        {
            parentNode.Child = childNode;
        }

        return childNode;
    }

    private void RemoveNode()
    {
        currentNodesCount -= 1;

        var parentNode = model.Trail.TailNode.Value;
        var childNode = parentNode.Child;
        childNode.Parent = null;
        parentNode.Child = null;

        model.Trail.TailNode.Value = childNode;
    }

    private int GenerateNodeId()
    {
        return idCounter++;
    }
}
