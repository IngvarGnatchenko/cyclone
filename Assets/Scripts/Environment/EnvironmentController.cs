﻿using System.Collections.Generic;
using UnityEngine;

public class EnvironmentController
{
    private const float CycledInfinity = 1000.0f;

    private EnvironmentModel model;

    private IntersectionController annihilatorController;

    private List<FuelModel> temp = new List<FuelModel>();
    private List<WallController> wallControllers = new List<WallController>();
    private WallControllerFactory wallFactory = new WallControllerFactory();

    public EnvironmentController(EnvironmentModel model, IntersectionController annihilatorController)
    {
        this.model = model;
        this.annihilatorController = annihilatorController;
    }

    public void Init()
    {
        annihilatorController.IntersectionFound += OnIntersectionFound;

        foreach (var wall in model.Walls)
        {
            var wallController = wallFactory.GetControllerFor(wall);

            wallController.Init();

            wallControllers.Add(wallController);
        }
    }

    public void Deinit()
    {
        annihilatorController.IntersectionFound -= OnIntersectionFound;

        foreach (var wallController in wallControllers)
        {
            wallController.Deinit();
        }

        wallControllers.Clear();
    }

    public void Update()
    {
        wallControllers.ForEach(controller => controller.Update());
    }

    private void OnIntersectionFound(IntersectionData intersectionData)
    {
        temp.Clear();

        foreach (var fuel in model.Fuels)
        {
            if (IsFuelCycloned(intersectionData, fuel))
            {
                temp.Add(fuel);
            }
        }

        foreach (var fuel in temp)
        {
            model.Fuels.Remove(fuel);
            model.PublishFuelTaken(fuel);

        }

        model.PublishComboMade(temp.Count);
    }

    private bool IsFuelCycloned(IntersectionData intersectionData, FuelModel fuel)
    {
        var counter = 0;

        var p1 = fuel.Content.Position;
        var p2 = new Vector2(CycledInfinity, fuel.Content.Position.y);

        var p3 = intersectionData.Intersection;
        var p4 = intersectionData.Intersector.Parent.Position;

        if (GeometryHelper.CheckIntersection(p1, p2, p3, p4, out var intersection0))
        {
            counter++;
        }

        var runningNode = intersectionData.Intersector.Parent;

        while (runningNode.Id != intersectionData.Intersectee.Id)
        {
            p3 = runningNode.Position;
            p4 = runningNode.Parent.Position;

            if (GeometryHelper.CheckIntersection(p1, p2, p3, p4, out var intersection1))
            {
                counter++;
            }

            runningNode = runningNode.Parent;
        }

        p3 = intersectionData.Intersectee.Position;
        p4 = intersectionData.Intersection;

        if (GeometryHelper.CheckIntersection(p1, p2, p3, p4, out var intersection2))
        {
            counter++;
        }

        return counter % 2 != 0;
    }
}
