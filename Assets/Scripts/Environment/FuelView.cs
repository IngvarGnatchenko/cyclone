﻿using UnityEngine;

public class FuelView : MonoBehaviour
{
    [SerializeField] private Transform anchor;
    [SerializeField] private Transform normalIcon;
    [SerializeField] private Transform boostIcon;
    [SerializeField] private Transform armorIcon;

    public FuelModel Model { get; private set; }

    public void Init(FuelModel model)
    {
        Model = model;

        anchor.localPosition = model.Content.Position;

        HandleIcon();
    }

    public void Deinit()
    {
    }

    private void HandleIcon()
    {
        normalIcon.gameObject.SetActive(Model.Content.Type == FuelType.Normal);
        boostIcon.gameObject.SetActive(Model.Content.Type == FuelType.Boost);
        armorIcon.gameObject.SetActive(Model.Content.Type == FuelType.Armor);
    }
}
