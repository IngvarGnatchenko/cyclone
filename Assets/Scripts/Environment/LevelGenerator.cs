﻿using System.Collections.Generic;

public class LevelGenerator
{
    private int fuelCounter;

    public List<FuelModel> GenerateFuels(LevelContent levelContent)
    {
        var fuels = new List<FuelModel>();

        foreach (var fuelContent in levelContent.FuelContents)
        {
            var fuelModel = new FuelModel(fuelCounter++, fuelContent);

            fuels.Add(fuelModel);
        }

        return fuels;
    }

    public List<CannonModel> GenerateCannons(LevelContent levelContent)
    {
        var cannons = new List<CannonModel>();

        foreach (var cannonContent in levelContent.CannonContents)
        {
            var cannonModel = new CannonModel(cannonContent);

            cannons.Add(cannonModel);
        }

        return cannons;
    }

    public List<HoleModel> GenerateHoles(LevelContent levelContent)
    {
        var holes = new List<HoleModel>();

        foreach (var holeContent in levelContent.HoleContents)
        {
            var hole = new HoleModel(holeContent);

            holes.Add(hole);
        }

        return holes;
    }

    public List<MineModel> GenerateMines(LevelContent levelContent)
    {
        var mines = new List<MineModel>();

        foreach (var mineContent in levelContent.MineContents)
        {
            var mine = new MineModel(mineContent);

            mines.Add(mine);
        }

        return mines;
    }

    public List<WallModel> GenerateWalls(LevelContent levelContent)
    {
        var walls = new List<WallModel>();

        foreach (var wallContent in levelContent.WallContents)
        {
            var wallModel = new WallModel(wallContent);

            walls.Add(wallModel);
        }

        return walls;
    }
}
