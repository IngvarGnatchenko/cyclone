﻿using System.Collections.Generic;
using UnityEngine;

public class EnvironmentView : MonoBehaviour
{
    [SerializeField] private FuelView viewPrefab;
    [SerializeField] private WallView wallPrefab;
    [SerializeField] private Transform viewAnchor;
    [SerializeField] private Transform wallAnchor;
    [SerializeField] private SpriteRenderer background;

    private EnvironmentModel model;

    private List<FuelView> views = new List<FuelView>();
    private List<WallView> walls = new List<WallView>();

    public void Init(EnvironmentModel model)
    {
        this.model = model;

        model.FuelTaken += OnFuelTaken;

        background.sprite = model.Background;

        foreach (var fuel in model.Fuels)
        {
            var view = GetView();

            view.Init(fuel);

            views.Add(view);
        }

        foreach (var wall in model.Walls)
        {
            var view = GetWallView();

            view.Init(wall);

            walls.Add(view);
        }
    }

    public void Deinit()
    {
        model.FuelTaken -= OnFuelTaken;

        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();

        foreach (var view in walls)
        {
            view.Deinit();

            DestroyView(view);
        }

        walls.Clear();
    }

    private WallView GetWallView()
    {
        return GameObject.Instantiate(wallPrefab, wallAnchor);
    }

    private FuelView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(MonoBehaviour view)
    {
        GameObject.Destroy(view.gameObject);
    }

    private void OnFuelTaken(FuelModel fuel)
    {
        var view = views.Find(item => item.Model.Id == fuel.Id);

        views.Remove(view);

        view.Deinit();

        DestroyView(view);
    }
}
