﻿public class FuelModel
{
    public int Id { get; }
    public FuelContent Content { get; }

    public FuelModel(int id, FuelContent content)
    {
        Id = id;
        Content = content;
    }
}
