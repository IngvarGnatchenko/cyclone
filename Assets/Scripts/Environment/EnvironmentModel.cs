﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentModel
{
    public event Action<FuelModel> FuelTaken;
    public event Action<int> ComboMade;

    public List<FuelModel> Fuels { get; }
    public List<WallModel> Walls { get; }

    public Sprite Background { get; }

    public EnvironmentModel(List<FuelModel> fuelModels, List<WallModel> wallModels, Sprite background)
    {
        Fuels = fuelModels;
        Walls = wallModels;

        Background = background;
    }

    public void PublishFuelTaken(FuelModel model)
    {
        FuelTaken?.Invoke(model);
    }

    public void PublishComboMade(int count)
    {
        ComboMade?.Invoke(count);
    }
}