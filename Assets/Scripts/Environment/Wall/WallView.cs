﻿using UnityEngine;

public class WallView : MonoBehaviour
{
    [SerializeField] private Transform positionAnchor;

    private WallModel model;

    public void Init(WallModel model)
    {
        this.model = model;

        model.Position.Changed += OnPositionChanged;

        OnPositionChanged(model.Position.Value);
    }

    public void Deinit()
    {
        model.Position.Changed -= OnPositionChanged;
    }

    private void OnPositionChanged(Vector2 position)
    {
        positionAnchor.localPosition = position;
    }
}
