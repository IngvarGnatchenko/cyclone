﻿public class WallController
{
    protected WallModel model;

    public WallController(WallModel model)
    {
        this.model = model;
    }

    public virtual void Init()
    {
        model.Position.Value = model.Content.Position;
    }

    public virtual void Deinit()
    {

    }

    public virtual void Update()
    {

    }
}
