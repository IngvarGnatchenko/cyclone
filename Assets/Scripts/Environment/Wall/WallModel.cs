﻿using UnityEngine;

public class WallModel
{
    public ReactiveWrapper<Vector2> Position = new ReactiveWrapper<Vector2>();
    
    public WallContent Content { get; }

    public WallModel(WallContent content)
    {
        Content = content;
    }
}
