﻿using UnityEngine;

public class WallControllerFactory
{
    public WallController GetControllerFor(WallModel model)
    {
        switch (model.Content.Type)
        {
            case WallType.Static:
                return new WallController(model);
            case WallType.HorizontalDynamic:
                return new WallLineController(
                    model,
                    new Vector2(model.Content.Amplitude, 0.0f),
                    GameContent.Instance.WallMoveCurve,
                    model.Content.TotalTime,
                    model.Content.TimeShift,
                    model.Content.Direction);
            case WallType.VerticalDynamic:
                return new WallLineController(
                    model,
                    new Vector2(0.0f, model.Content.Amplitude),
                    GameContent.Instance.WallMoveCurve,
                    model.Content.TotalTime,
                    model.Content.TimeShift,
                    model.Content.Direction);
            case WallType.Cicular:
                return new WallCircularController(
                    model,
                    model.Content.Amplitude,
                    model.Content.TotalTime,
                    model.Content.TimeShift,
                    model.Content.Direction);
        }

        return new WallController(model);
    }
}
