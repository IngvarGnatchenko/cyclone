﻿using UnityEngine;

public class WallLineController : WallController
{
    private Vector2 amplitude;
    private AnimationCurve curve;
    private readonly float totalTime;
    private readonly float timeShift;
    private readonly int direction;
    private float currentTime;

    public WallLineController(WallModel model, Vector2 amplitude, AnimationCurve curve, float totalTime, float timeShift, int direction) : base(model)
    {
        this.amplitude = amplitude;
        this.curve = curve;
        this.totalTime = totalTime;
        this.timeShift = timeShift;
        this.direction = direction;
    }

    public override void Init()
    {
        base.Init();

        Step(0.0f);
    }

    public override void Deinit()
    {
        base.Deinit();
    }

    public override void Update()
    {
        base.Update();

        currentTime += Time.deltaTime;

        Step(currentTime);
    }

    private void Step(float runningTime)
    {
        runningTime += timeShift;

        runningTime = runningTime % totalTime;

        var ratio = runningTime / totalTime;

        var evalRatio = curve.Evaluate(ratio);

        model.Position.Value = model.Content.Position + direction * amplitude * evalRatio;
    }
}
