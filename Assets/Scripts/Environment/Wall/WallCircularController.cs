﻿using UnityEngine;

public class WallCircularController : WallController
{
    private readonly float amplitude;
    private readonly float totalTime;
    private readonly float timeShift;
    private readonly int direction;

    private float currentTime;

    public WallCircularController(WallModel model, float amplitude, float totalTime, float timeShift, int direction) : base(model)
    {
        this.amplitude = amplitude;
        this.totalTime = totalTime;
        this.timeShift = timeShift;
        this.direction = direction;
    }

    public override void Init()
    {
        base.Init();

        Step(currentTime);
    }

    public override void Deinit()
    {
        base.Deinit();
    }

    public override void Update()
    {
        base.Update();

        currentTime += direction * Time.deltaTime;

        Step(currentTime);
    }

    private void Step(float runningTime)
    {
        runningTime += timeShift;

        runningTime = runningTime % totalTime;

        var ratio = runningTime / totalTime;

        var angle = ratio * 360;

        var x = Mathf.Cos(angle * Mathf.Deg2Rad);
        var y = Mathf.Sin(angle * Mathf.Deg2Rad);

        var position = new Vector2(x, y);

        model.Position.Value = model.Content.Position + position * amplitude / 2.0f;
    }
}
