﻿using UnityEngine;

public class FuelEditorView : MonoBehaviour
{
    public FuelContent Content { get; private set; } = new FuelContent();

    public void Init(FuelContent content)
    {
        Content = content;

        transform.localPosition = content.Position;
    }

    public void Deinit()
    {

    }

    public void Flush()
    {
        Content.Position = transform.localPosition;
    }
}
