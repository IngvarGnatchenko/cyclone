﻿using UnityEngine;

public class MineEditorView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer gizmo;

    public MineContent Content { get; private set; } = new MineContent();

    public void Init(MineContent content)
    {
        Content = content;

        transform.localPosition = content.Position;

        ExternalUpdate();
    }

    public void Deinit()
    {
    }

    public void Flush()
    {
        Content.Position = transform.localPosition;
    }

    public void ExternalUpdate()
    {
        gizmo.transform.localScale = Vector3.one * Content.ActivationRadius * 2.0f;
    }
}

