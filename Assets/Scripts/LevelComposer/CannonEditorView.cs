﻿using UnityEngine;

public class CannonEditorView : MonoBehaviour
{
    public CannonContent Content { get; private set; } = new CannonContent();

    public void Init(CannonContent content)
    {
        Content = content;

        transform.localPosition = content.Position;
        transform.localRotation = UIUtils.GetQuaternionFromVector(content.Direction);
    }

    public void Deinit()
    {

    }

    public void Flush()
    {
        Content.Position = transform.localPosition;
        Content.Direction = UIUtils.GetVectorFromQuaternion(transform.localRotation);
    }
}

