﻿using System.Collections.Generic;
using UnityEngine;

public class LevelSceneSource : MonoBehaviour
{
    [SerializeField] private Transform fuelAnchor;
    [SerializeField] private Transform cannonAnchor;
    [SerializeField] private Transform holeAnchor;
    [SerializeField] private Transform mineAnchor;
    [SerializeField] private Transform wallAnchor;
    [SerializeField] private FuelEditorView fuelPrefab;
    [SerializeField] private CannonEditorView cannonPrefab;
    [SerializeField] private HoleEditorView holePrefab;
    [SerializeField] private MineEditorView minePrefab;
    [SerializeField] private WallEditorView wallPrefab;

    private List<GameObject> temp = new List<GameObject>();

    public void Init(LevelContent levelContent)
    {
        foreach (var fuelContent in levelContent.FuelContents)
        {
            var fuelView = Instantiate(fuelPrefab, fuelAnchor);

            fuelView.Init(fuelContent);

            fuelView.gameObject.name = "fuel";
        }

        foreach (var cannonContent in levelContent.CannonContents)
        {
            var cannonView = Instantiate(cannonPrefab, cannonAnchor);

            cannonView.Init(cannonContent);

            cannonView.gameObject.name = "cannon";
        }

        foreach (var holeContent in levelContent.HoleContents)
        {
            var holeView = Instantiate(holePrefab, holeAnchor);

            holeView.Init(holeContent);

            holeView.gameObject.name = "hole";
        }

        foreach (var mineContent in levelContent.MineContents)
        {
            var mineView = Instantiate(minePrefab, mineAnchor);

            mineView.Init(mineContent);

            mineView.gameObject.name = "mine";
        }

        foreach (var wallContent in levelContent.WallContents)
        {
            var wallView = Instantiate(wallPrefab, wallAnchor);

            wallView.Init(wallContent);

            wallView.gameObject.name = "wall";
        }
    }

    public void Deinit()
    {
        temp.Clear();

        foreach (var fuelView in fuelAnchor.GetComponentsInChildren<FuelEditorView>())
        {
            fuelView.Deinit();

            temp.Add(fuelView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        temp.Clear();

        foreach (var cannonView in cannonAnchor.GetComponentsInChildren<CannonEditorView>())
        {
            cannonView.Deinit();

            temp.Add(cannonView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        temp.Clear();

        foreach (var holeView in holeAnchor.GetComponentsInChildren<HoleEditorView>())
        {
            holeView.Deinit();

            temp.Add(holeView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        temp.Clear();

        foreach (var mineView in mineAnchor.GetComponentsInChildren<MineEditorView>())
        {
            mineView.Deinit();

            temp.Add(mineView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        temp.Clear();

        foreach (var wallView in wallAnchor.GetComponentsInChildren<WallEditorView>())
        {
            wallView.Deinit();

            temp.Add(wallView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));
    }

    public LevelContent CopyLevel()
    {
        var fuelContents = new List<FuelContent>();
        var cannonContents = new List<CannonContent>();
        var holeContents = new List<HoleContent>();
        var mineContents = new List<MineContent>();
        var wallContents = new List<WallContent>();

        foreach (Transform child in fuelAnchor.transform)
        {
            var fuelView = child.GetComponent<FuelEditorView>();

            fuelView.Flush();

            fuelContents.Add(fuelView.Content);
        }

        foreach (Transform child in cannonAnchor.transform)
        {
            var cannonView = child.GetComponent<CannonEditorView>();

            cannonView.Flush();

            cannonContents.Add(cannonView.Content);
        }

        foreach (Transform child in holeAnchor.transform)
        {
            var holeView = child.GetComponent<HoleEditorView>();

            holeView.Flush();

            holeContents.Add(holeView.Content);
        }

        foreach (Transform child in mineAnchor.transform)
        {
            var mineView = child.GetComponent<MineEditorView>();

            mineView.Flush();

            mineContents.Add(mineView.Content);
        }

        foreach (Transform child in wallAnchor.transform)
        {
            var wallView = child.GetComponent<WallEditorView>();

            wallView.Flush();

            wallContents.Add(wallView.Content);
        }

        return new LevelContent()
        {
            FuelContents = fuelContents,
            CannonContents = cannonContents,
            HoleContents = holeContents,
            MineContents = mineContents,
            WallContents = wallContents
        };
    }
}

