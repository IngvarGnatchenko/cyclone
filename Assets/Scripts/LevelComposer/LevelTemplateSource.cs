﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class LevelTemplateSource : MonoBehaviour
{
    [SerializeField] private Vector2 gameArea;
    [SerializeField] private int cannonCount;
    [SerializeField] private int holeCount;
    [SerializeField] private int fuelCount;
    [SerializeField] private int wallCount;
    [SerializeField] private int mineCount;
    [SerializeField] private List<GameObject> cannonPresets;
    [SerializeField] private List<GameObject> holePresets;
    [SerializeField] private List<GameObject> fuelPresets;
    [SerializeField] private List<GameObject> wallPresets;
    [SerializeField] private List<GameObject> minePresets;

    [SerializeField] private Transform fuelAnchor;
    [SerializeField] private Transform cannonAnchor;
    [SerializeField] private Transform holeAnchor;
    [SerializeField] private Transform wallAnchor;
    [SerializeField] private Transform mineAnchor;

    private List<GameObject> temp = new List<GameObject>();

    public void GenerateLevel(GenerateTarget target)
    {
        switch (target)
        {
            case GenerateTarget.Cannon:
                FillWithCannons();
                break;
            case GenerateTarget.Hole:
                FillWithHoles();
                break;
            case GenerateTarget.Fuel:
                FillWithFuels();
                break;
            case GenerateTarget.Wall:
                FillWithWalls();
                break;
            case GenerateTarget.Mine:
                FillWithMines();
                break;
        }
    }

    private void FillWithCannons()
    {
        temp.Clear();

        foreach (var cannonView in cannonAnchor.GetComponentsInChildren<CannonEditorView>())
        {
            cannonView.Deinit();

            temp.Add(cannonView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        LocateItems(cannonAnchor, cannonCount, cannonPresets);

        foreach (var cannonView in cannonAnchor.GetComponentsInChildren<CannonEditorView>())
        {
            cannonView.Init(new CannonContent()
            {
                Position = cannonView.transform.localPosition
            });

            cannonView.gameObject.name = "cannon";
        }
    }

    private void FillWithHoles()
    {
        temp.Clear();

        foreach (var holeView in holeAnchor.GetComponentsInChildren<HoleEditorView>())
        {
            holeView.Deinit();

            temp.Add(holeView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        LocateItems(holeAnchor, holeCount, holePresets);

        foreach (var holeView in holeAnchor.GetComponentsInChildren<HoleEditorView>())
        {
            holeView.Init(new HoleContent()
            {
                Position = holeView.transform.localPosition
            });

            holeView.gameObject.name = "hole";
        }
    }

    private void FillWithFuels()
    {
        temp.Clear();

        foreach (var fuelView in fuelAnchor.GetComponentsInChildren<FuelEditorView>())
        {
            fuelView.Deinit();

            temp.Add(fuelView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        LocateItems(fuelAnchor, fuelCount, fuelPresets);

        foreach (var fuelView in fuelAnchor.GetComponentsInChildren<FuelEditorView>())
        {
            fuelView.Init(new FuelContent()
            {
                Position = fuelView.transform.localPosition
            });

            fuelView.gameObject.name = "fuel";
        }
    }

    private void LocateItems(Transform anchor, int count, List<GameObject> presets)
    {
        for (var i = 0; i < count; i++)
        {
            var randomPreset = presets[UnityEngine.Random.Range(0, presets.Count)];

            var randomInstance = Instantiate(randomPreset, anchor);

            var randomPosition = GetRandomPosition();

            randomInstance.transform.localPosition = randomPosition;

            temp.Clear();

            foreach (Transform child in randomInstance.transform)
            {
                temp.Add(child.gameObject);
            }

            temp.ForEach(item => item.transform.SetParent(anchor, true));

            DestroyImmediate(randomInstance);
        }
    }

    private void FillWithWalls()
    {
        temp.Clear();

        foreach (var wallView in wallAnchor.GetComponentsInChildren<WallEditorView>())
        {
            wallView.Deinit();

            temp.Add(wallView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        LocateItems(wallAnchor, wallCount, wallPresets);

        foreach (var wallView in wallAnchor.GetComponentsInChildren<WallEditorView>())
        {
            wallView.Init(new WallContent()
            {
                Position = wallView.transform.localPosition
            });

            wallView.gameObject.name = "wall";
        }
    }

    private void FillWithMines()
    {
        temp.Clear();

        foreach (var mineView in mineAnchor.GetComponentsInChildren<MineEditorView>())
        {
            mineView.Deinit();

            temp.Add(mineView.gameObject);
        }

        temp.ForEach(item => DestroyImmediate(item));

        LocateItems(mineAnchor, mineCount, minePresets);

        foreach (var mineView in wallAnchor.GetComponentsInChildren<MineEditorView>())
        {
            mineView.Init(new MineContent()
            {
                Position = mineView.transform.localPosition
            });

            mineView.gameObject.name = "mine";
        }
    }

    private Vector2 GetRandomPosition()
    {
        var x = Mathf.RoundToInt(UnityEngine.Random.Range(-gameArea.x, gameArea.x));
        var y = Mathf.RoundToInt(UnityEngine.Random.Range(-gameArea.y, gameArea.y));

        return new Vector2(x, y);
    }
}

public enum GenerateTarget
{
    Cannon,
    Hole,
    Fuel,
    Wall,
    Mine
}

