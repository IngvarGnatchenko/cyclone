﻿using System.Collections.Generic;
using UnityEngine;

public class LevelContentSource : MonoBehaviour
{
    [SerializeField] private GameContent gameContent;

    public int LevelCount => gameContent.LevelContents.Count;

    public LevelContent InsertLevel(int levelIndex)
    {
        var levelContent = new LevelContent()
        {
            FuelContents = new List<FuelContent>(),
            CannonContents = new List<CannonContent>(),
            HoleContents = new List<HoleContent>(),
            MineContents = new List<MineContent>(),
            WallContents = new List<WallContent>()
        };

        gameContent.LevelContents.Insert(levelIndex, levelContent);

        SetDirty();

        return levelContent;
    }

    public LevelContent ReadLevel(int levelIndex)
    {
        if (gameContent.LevelContents.Count > levelIndex && levelIndex >= 0)
        {
            return gameContent.LevelContents[levelIndex];
        }

        return null;
    }

    public void UpdateLevel(int levelIndex, LevelContent levelContent)
    {
        if (gameContent.LevelContents.Count > levelIndex && levelIndex >= 0)
        {
            gameContent.LevelContents[levelIndex].FuelContents = levelContent.FuelContents;
            gameContent.LevelContents[levelIndex].CannonContents = levelContent.CannonContents;
            gameContent.LevelContents[levelIndex].HoleContents = levelContent.HoleContents;
            gameContent.LevelContents[levelIndex].MineContents = levelContent.MineContents;
            gameContent.LevelContents[levelIndex].WallContents = levelContent.WallContents;

            SetDirty();
        }
    }

    public void DeleteLevel(int levelIndex)
    {
        if (gameContent.LevelContents.Count > levelIndex && levelIndex >= 0)
        {
            gameContent.LevelContents.RemoveAt(levelIndex);

            SetDirty();

            return;
        }
    }

    private void SetDirty()
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(gameContent);
#endif
    }
}

