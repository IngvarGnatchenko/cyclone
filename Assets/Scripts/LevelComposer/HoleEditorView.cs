﻿using UnityEngine;

public class HoleEditorView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer gizmo;

    public HoleContent Content { get; private set; } = new HoleContent();

    public void Init(HoleContent content)
    {
        Content = content;

        transform.localPosition = content.Position;

        ExternalUpdate();
    }

    public void Deinit()
    {
    }

    public void Flush()
    {
        Content.Position = transform.localPosition;
    }

    public void ExternalUpdate()
    {
        gizmo.transform.localScale = Vector3.one * Content.CaptureRadius * 2.0f;
        gizmo.color = new Color(255, 0.0f, 0.0f, 1.0f - Mathf.Clamp01(Content.CaptureForce));
    }
}

