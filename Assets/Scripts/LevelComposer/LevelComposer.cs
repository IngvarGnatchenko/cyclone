﻿using UnityEngine;

public class LevelComposer : MonoBehaviour
{
    [SerializeField] private LevelContentSource contentSource;
    [SerializeField] private LevelSceneSource sceneSource;
    [SerializeField] private LevelTemplateSource templateSource;

    public LevelContentSource ContentSource => contentSource;
    public LevelSceneSource SceneSource => sceneSource;
    public LevelTemplateSource TemplateSource => templateSource;

    public int LevelIndex { get; set; }
    public bool FirstLoaded { get; set; }
}

