﻿using UnityEngine;

public class WallEditorView : MonoBehaviour
{
    public WallContent Content { get; private set; } = new WallContent();

    public void Init(WallContent content)
    {
        Content = content;
        
        transform.localPosition = content.Position;
    }

    public void Deinit()
    {

    }

    public void Flush()
    {
        Content.Position = transform.localPosition;
    }
}
