﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HoleEditorView))]
public class HoleEditorViewEditor : Editor
{
    private HoleEditorView view;

    private void OnEnable()
    {
        view = target as HoleEditorView;
    }

    private void OnSceneGUI()
    {
        
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        view.Content.CaptureForce = Mathf.Clamp01(EditorGUILayout.FloatField("Force", view.Content.CaptureForce));
        view.Content.CaptureRadius = EditorGUILayout.FloatField("Radius", view.Content.CaptureRadius);

        view.ExternalUpdate();
    }
}