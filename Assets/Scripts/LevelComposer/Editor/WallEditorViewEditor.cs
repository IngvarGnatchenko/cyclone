﻿using UnityEditor;

[CustomEditor(typeof(WallEditorView))]
public class WallEditorViewEditor : Editor
{
    private WallEditorView view;
    private WallType[] values;

    private void OnEnable()
    {
        view = target as WallEditorView;
    }

    private void OnSceneGUI()
    {
        
    }

    public override void OnInspectorGUI()
    {
        view.Content.Type = (WallType)EditorGUILayout.EnumPopup("Type", view.Content.Type);
        view.Content.Direction = EditorGUILayout.IntField("Direction", view.Content.Direction);
        view.Content.Amplitude = EditorGUILayout.FloatField("Amplitude", view.Content.Amplitude);
        view.Content.TotalTime = EditorGUILayout.FloatField("TotalTime", view.Content.TotalTime);
        view.Content.TimeShift = EditorGUILayout.FloatField("TimeShift", view.Content.TimeShift);
    }
}
