﻿using UnityEditor;

[CustomEditor(typeof(FuelEditorView))]
public class FuelEditorViewEditor : Editor
{
    private FuelEditorView view;

    private void OnEnable()
    {
        view = target as FuelEditorView;
    }

    private void OnSceneGUI()
    {
        
    }

    public override void OnInspectorGUI()
    {
        view.Content.Type = (FuelType)EditorGUILayout.EnumPopup("Type", view.Content.Type);
        view.Content.Score = EditorGUILayout.IntField("Score", view.Content.Score);
    }
}
