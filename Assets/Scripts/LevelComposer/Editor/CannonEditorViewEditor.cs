﻿using UnityEditor;

[CustomEditor(typeof(CannonEditorView))]
public class CannonEditorViewEditor : Editor
{
    private CannonEditorView view;

    private void OnEnable()
    {
        view = target as CannonEditorView;
    }

    private void OnSceneGUI()
    {
        
    }

    public override void OnInspectorGUI()
    {
        view.Content.FireShift = EditorGUILayout.FloatField("Fire shift", view.Content.FireShift);
        view.Content.FirePeriod = EditorGUILayout.FloatField("Fire period", view.Content.FirePeriod);
    }
}