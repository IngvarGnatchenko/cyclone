﻿using UnityEditor;

[CustomEditor(typeof(MineEditorView))]
public class MineEditorViewEditor : Editor
{
    private MineEditorView view;

    private void OnEnable()
    {
        view = target as MineEditorView;
    }

    private void OnSceneGUI()
    {

    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        view.Content.ActivationRadius = EditorGUILayout.FloatField("Radius", view.Content.ActivationRadius);
        view.Content.ShellCount = EditorGUILayout.IntField("Count", view.Content.ShellCount);
        view.Content.ShellSpeed = EditorGUILayout.FloatField("Speed", view.Content.ShellSpeed);

        view.ExternalUpdate();
    }
}