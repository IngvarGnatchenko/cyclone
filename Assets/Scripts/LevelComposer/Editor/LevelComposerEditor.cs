﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelComposer))]
public class LevelComposerEditor : Editor
{
    private LevelComposer levelComposer;

    private void OnEnable()
    {
        levelComposer = target as LevelComposer;

        if (!levelComposer.FirstLoaded)
        {
            levelComposer.FirstLoaded = true;

            OnLevelIndexChanged(-1, levelComposer.LevelIndex);
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        DrawCounter();

        DrawButtons();

        DrawTemplate();
    }

    private void DrawCounter()
    {
        var options = Enumerable.Range(0, levelComposer.ContentSource.LevelCount).Reverse().ToArray();

        var visuals = options.ToList().ConvertAll(option => option.ToString()).ToArray();

        var prevLevelIndex = levelComposer.LevelIndex;

        levelComposer.LevelIndex = EditorGUILayout.IntPopup(levelComposer.LevelIndex, visuals, options);

        if (prevLevelIndex != levelComposer.LevelIndex)
        {
            OnLevelIndexChanged(prevLevelIndex, levelComposer.LevelIndex);
        }
    }

    private void DrawButtons()
    {
        if (GUILayout.Button("Insert Level"))
        {
            InsertLevel();
        }

        if (GUILayout.Button("Remove Level"))
        {
            DeleteLevel();
        }
    }

    private void DrawTemplate()
    {
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        if (GUILayout.Button("Generate Fuels"))
        {
            GenerateLevel(GenerateTarget.Fuel);
        }

        if (GUILayout.Button("Generate Walls"))
        {
            GenerateLevel(GenerateTarget.Wall);
        }

        if (GUILayout.Button("Generate Cannons"))
        {
            GenerateLevel(GenerateTarget.Cannon);
        }

        if (GUILayout.Button("Generate Holes"))
        {
            GenerateLevel(GenerateTarget.Hole);
        }

        if (GUILayout.Button("Generate Mines"))
        {
            GenerateLevel(GenerateTarget.Mine);
        }
    }

    private void OnLevelIndexChanged(int prevIndex, int currentIndex)
    {
        var prevContent = levelComposer.SceneSource.CopyLevel();

        levelComposer.ContentSource.UpdateLevel(prevIndex, prevContent);

        var newContent = levelComposer.ContentSource.ReadLevel(currentIndex);

        Reload(newContent);
    }

    private void InsertLevel()
    {
        var prevLevelIndex = levelComposer.ContentSource.LevelCount > 0 ? levelComposer.LevelIndex : -2;

        var levelContent = levelComposer.ContentSource.InsertLevel(levelComposer.LevelIndex);

        OnLevelIndexChanged(prevLevelIndex + 1, levelComposer.LevelIndex);
    }

    private void DeleteLevel()
    {
        if (levelComposer.ContentSource.LevelCount <= 1)
        {
            return;
        }

        var levelIndex = levelComposer.LevelIndex;

        levelComposer.ContentSource.DeleteLevel(levelIndex);

        levelComposer.LevelIndex = Mathf.Clamp(levelIndex, 0, levelComposer.ContentSource.LevelCount - 1);

        OnLevelIndexChanged(-1, levelComposer.LevelIndex);
    }

    private void GenerateLevel(GenerateTarget target)
    {
        levelComposer.TemplateSource.GenerateLevel(target);
    }

    private void Reload(LevelContent levelContent)
    {
        levelComposer.SceneSource.Deinit();

        if (levelContent != null)
        {
            levelComposer.SceneSource.Init(levelContent);
        }
    }
}
