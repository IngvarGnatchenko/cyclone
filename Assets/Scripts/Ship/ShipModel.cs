﻿using System;
using UnityEngine;

public class ShipModel
{
    public event Action<WallView> Collided;
    public event Action Detonated;

    public ShipData Content { get; }

    public ReactiveWrapper<Vector2> Position = new ReactiveWrapper<Vector2>();
    public ReactiveWrapper<Vector2> Direction = new ReactiveWrapper<Vector2>();

    public ReactiveWrapper<bool> Boost = new ReactiveWrapper<bool>();
    public ReactiveWrapper<bool> Armor = new ReactiveWrapper<bool>();

    public ShipModel(ShipData content)
    {
        Content = content;
    }

    public void PublishCollided(WallView wallView)
    {
        Collided?.Invoke(wallView);
    }

    public void PublishDetonated()
    {
        Detonated?.Invoke();
    }
}
