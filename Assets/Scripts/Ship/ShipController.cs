﻿using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private PointerJoystick gameJoystick;
    [SerializeField] private TutorialJoystick tutorialJoystick;

    private ShipModel model;
    private List<HoleModel> holes;
    private TimeSystem timeSystem;
    private Rect screenBorderInUnits;
    private Vector2 targetDirection;

    private float boostLastTime;
    private float armorLastTime;
    private float boostFactor;

    private bool isTutorial;

    public void Init(ShipModel model, List<HoleModel> holes, TimeSystem timeSystem, Rect screenBorderInUnits, bool isTutorial)
    {
        boostLastTime = 0;
        armorLastTime = 0;

        this.model = model;
        this.holes = holes;
        this.timeSystem = timeSystem;
        this.screenBorderInUnits = screenBorderInUnits;
        this.isTutorial = isTutorial;

        model.Boost.Changed += OnBoostChanged;

        tutorialJoystick.gameObject.SetActive(isTutorial);
        gameJoystick.gameObject.SetActive(!isTutorial);

        if (isTutorial)
        {
            tutorialJoystick.DirectionChanged += OnDirectionChanged;
            tutorialJoystick.Init(model);
        }
        else
        {
            gameJoystick.DirectionChanged += OnDirectionChanged;
            gameJoystick.Init();
        }
        
        model.Direction.Value = Vector2.up;
        model.Position.Value = spawnPoint.transform.localPosition;

        OnBoostChanged(model.Boost.Value);
        OnDirectionChanged(Vector2.up);
    }

    public void Deinit()
    {
        model.Boost.Changed -= OnBoostChanged;

        if (isTutorial)
        {
            tutorialJoystick.DirectionChanged -= OnDirectionChanged;
            tutorialJoystick.Deinit();
        }
        else
        {
            gameJoystick.DirectionChanged -= OnDirectionChanged;
            gameJoystick.Deinit();
        }
    }

    public void ActivateBoost()
    {
        boostLastTime = timeSystem.CurrentTime.Value;
    }

    public void ActivateArmor()
    {
        armorLastTime = timeSystem.CurrentTime.Value;
    }

    private void OnDirectionChanged(Vector2 direction)
    {
        targetDirection = direction;
    }

    public void ExternalUpdate()
    {
        UpdatePosition();
        UpdateRotation();

        CheckBonuses();

        if (isTutorial)
        {
            tutorialJoystick.ExternalUpdate();
        }
    }

    private void OnBoostChanged(bool state)
    {
        boostFactor = state ? GameContent.Instance.BoostFactor : 1.0f;
    }

    private void UpdatePosition()
    {
        var currentPosition = model.Position.Value;

        var suckingFactor = GetSuckingFactor();

        currentPosition += model.Direction.Value * GameContent.Instance.PositionSpeed * suckingFactor * boostFactor * Time.deltaTime;

        model.Position.Value = ClampPosition(currentPosition);
    }

    private void UpdateRotation()
    {
        model.Direction.Value = Vector3.RotateTowards(model.Direction.Value, targetDirection, GameContent.Instance.RotationSpeed * Time.deltaTime, 0.0f);
    }

    private void CheckBonuses()
    {
        if (IsBoostActive() && !model.Boost.Value)
        {
            model.Boost.Value = true;
        }

        if (!IsBoostActive() && model.Boost.Value)
        {
            model.Boost.Value = false;
        }

        if (IsArmorActive() && !model.Armor.Value)
        {
            model.Armor.Value = true;
        }

        if (!IsArmorActive() && model.Armor.Value)
        {
            model.Armor.Value = false;
        }
    }

    private bool IsBoostActive()
    {
        return boostLastTime + GameContent.Instance.BoostTime > timeSystem.CurrentTime.Value;
    }

    private bool IsArmorActive()
    {
        return armorLastTime + GameContent.Instance.ArmorTime > timeSystem.CurrentTime.Value;
    }

    private float GetSuckingFactor()
    {
        var suckingFactor = 1.0f;

        foreach (var hole in holes)
        {
            if (hole.Capturing.Value)
            {
                suckingFactor = suckingFactor * hole.Content.CaptureForce;
            }
        }

        return suckingFactor;
    }

    private Vector2 ClampPosition(Vector2 inputPosition)
    {
        var x = Mathf.Clamp(inputPosition.x, screenBorderInUnits.min.x, screenBorderInUnits.max.x);
        var y = Mathf.Clamp(inputPosition.y, screenBorderInUnits.min.y, screenBorderInUnits.max.y);

        return new Vector2(x, y);
    }
}
