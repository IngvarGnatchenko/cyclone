﻿using System;
using UnityEngine;

public class ShipView : MonoBehaviour
{
    [SerializeField] private Transform positionAnchor;
    [SerializeField] private Transform rotationAnchor;
    [SerializeField] private Transform visual;
    [SerializeField] private Transform boostVisual;
    [SerializeField] private Transform armorVisual;

    private ShipModel model;

    private GameObject shipModelInstance;

    public void Init(ShipModel model)
    {
        this.model = model;

        model.Position.Changed += OnPositionChanged;
        model.Direction.Changed += OnDirectionChanged;

        model.Boost.Changed += OnBoostChanged;
        model.Armor.Changed += OnArmorChanged;

        model.Collided += OnCollided;
        model.Detonated += OnDetonated;

        shipModelInstance = GameObject.Instantiate(model.Content.model, rotationAnchor);

        OnPositionChanged(model.Position.Value);
        OnDirectionChanged(model.Direction.Value);

        OnBoostChanged(model.Boost.Value);
        OnArmorChanged(model.Armor.Value);

        visual.gameObject.SetActive(true);
    }

    public void Deinit()
    {
        model.Position.Changed -= OnPositionChanged;
        model.Direction.Changed -= OnDirectionChanged;

        model.Boost.Changed -= OnBoostChanged;
        model.Armor.Changed -= OnArmorChanged;

        model.Collided -= OnCollided;
        model.Detonated -= OnDetonated;

        GameObject.Destroy(shipModelInstance);
    }

    private void OnBoostChanged(bool state)
    {
        boostVisual.gameObject.SetActive(state);
    }

    private void OnArmorChanged(bool state)
    {
        armorVisual.gameObject.SetActive(state);
    }

    private void OnPositionChanged(Vector2 position)
    {
        positionAnchor.localPosition = position;
    }

    private void OnDirectionChanged(Vector2 direction)
    {
        rotationAnchor.localRotation = UIUtils.GetQuaternionFromVector(direction);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var wallView = collision.gameObject.GetComponent<WallView>();

        if (wallView != null && !model.Armor.Value)
        {
            model.PublishCollided(wallView);
        }
    }

    private void OnCollided(WallView wall)
    {
        visual.gameObject.SetActive(false);
    }

    private void OnDetonated()
    {
        visual.gameObject.SetActive(false);
    }
}
