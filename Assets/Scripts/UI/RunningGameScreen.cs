﻿using UnityEngine;

public class RunningGameScreen : UIWindow
{
    [SerializeField] private ProgressView progressView;
    [SerializeField] private ComboView comboView;
    [SerializeField] private Transform tutorialAnchor;

    public void Init(GameModel gameModel, UserModel userModel)
    {
        progressView.Init(gameModel, userModel);
        comboView.Init(gameModel.Level);

        tutorialAnchor.gameObject.SetActive(userModel.IsTutorial);
    }

    public void ExternalUpdate()
    {
        progressView.ExternalUpdate();
    }

    public void Deinit()
    {
        progressView.Deinit();
        comboView.Deinit();
    }
}
