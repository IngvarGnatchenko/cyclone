﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryGameScreen : UIWindow
{
    [SerializeField] private Button activeButton;
    [SerializeField] private TextMeshProUGUI completedText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private AudioSource victorySound;

    private const string CompletedFormat = "LEVEL {0}\ncompleted!";
    private const string ScoreFormat = "Your score is\n<size=200%>{0}</size>";

    private TaskCompletionSource<object> tcs;

    public void Init()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    public void Deinit()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    public Task Wait(int levelIndex, int score)
    {
        tcs = new TaskCompletionSource<object>();

        completedText.text = string.Format(CompletedFormat, levelIndex + 1);
        scoreText.text = string.Format(ScoreFormat, score);

        victorySound.Play();

        return tcs.Task;
    }

    private void ActiveButtonHandler()
    {
        tcs.SetResult(null);
    }
}
