﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnlockGameScreen : UIWindow
{
    [SerializeField] private Button activeButton;
    [SerializeField] private Image shipIcon;
    [SerializeField] private TextMeshProUGUI shipNameText;
    [SerializeField] private AudioSource victorySound;

    private TaskCompletionSource<object> tcs;

    public void Init()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    public void Deinit()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    public async Task Show(ShipData shipData)
    {
        tcs = new TaskCompletionSource<object>();

        shipIcon.sprite = shipData.icon;
        shipNameText.text = shipData.name;

        victorySound.Play();

        activeButton.gameObject.SetActive(false);

        await Task.Delay(AwaitTimeMs);

        if (activeButton != null)
        {
            activeButton.gameObject.SetActive(true);
        }

        await tcs.Task;
    }

    private void ActiveButtonHandler()
    {
        tcs.SetResult(null);
    }
}
