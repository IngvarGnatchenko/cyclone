﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currentLevelText;
    [SerializeField] private TextMeshProUGUI nextLevelText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Image progressImage;

    private const float SmoothChangeSpeed = 1.0f;

    private GameModel gameModel;
    private UserModel userModel;

    private float targetProgressRatio;
    private float currentProgressRatio;

    private ValueMorpher morpher = new ValueMorpher();

    public void Init(GameModel gameModel, UserModel userModel)
    {
        this.gameModel = gameModel;
        this.userModel = userModel;

        currentLevelText.text = (userModel.LevelIndex.Value).ToString();
        nextLevelText.text = (userModel.LevelIndex.Value + 1).ToString();

        gameModel.Score.Changed += OnScoreChanged;
        userModel.SessionScore.Changed += OnSessionScoreChanged;
        morpher.ValueChanged += OnMorpherValueChanged;

        OnScoreChanged(gameModel.Score.Value);
        OnSessionScoreChanged(userModel.SessionScore.Value);
    }

    public void ExternalUpdate()
    {
        morpher.Update();

        currentProgressRatio = Mathf.MoveTowards(currentProgressRatio, targetProgressRatio, SmoothChangeSpeed * Time.deltaTime);

        progressImage.fillAmount = currentProgressRatio;
    }

    public void Deinit()
    {
        gameModel.Score.Changed -= OnScoreChanged;
        userModel.SessionScore.Changed -= OnSessionScoreChanged;
        morpher.ValueChanged -= OnMorpherValueChanged;
    }

    private void OnSessionScoreChanged(int sessionScore)
    {
        morpher.Start(sessionScore);
    }

    private void OnScoreChanged(int score)
    {
        targetProgressRatio = (float)score / (float)gameModel.WinScore;
    }

    private void OnMorpherValueChanged(long value)
    {
        scoreText.text = value.ToString();
    }
}
