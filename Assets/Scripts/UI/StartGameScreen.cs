﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartGameScreen : UIWindow
{
    [SerializeField] private Button activeButton;
    [SerializeField] private TextMeshProUGUI scoreText;

    private const string ScoreFormat = "Best score\n<size=200%>{0}</size>";

    private TaskCompletionSource<object> tcs;

    public void Init()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    public void Deinit()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    public Task Wait(int score)
    {
        tcs = new TaskCompletionSource<object>();

        scoreText.text = string.Format(ScoreFormat, score);

        return tcs.Task;
    }

    private void ActiveButtonHandler()
    {
        tcs.SetResult(null);
    }
}
