﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class TutorialGameScreen : UIWindow
{
    [SerializeField] private Button activeButton;

    private TaskCompletionSource<object> tcs;

    public void Init()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    public void Deinit()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    public async Task Wait()
    {
        tcs = new TaskCompletionSource<object>();

        activeButton.gameObject.SetActive(false);

        await Task.Delay(AwaitTimeMs);

        if (activeButton != null)
        {
            activeButton.gameObject.SetActive(true);
        }

        await tcs.Task;
    }

    private void ActiveButtonHandler()
    {
        tcs.SetResult(null);
    }
}
