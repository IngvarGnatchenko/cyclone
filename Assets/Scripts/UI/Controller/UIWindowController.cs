﻿using System.Collections.Generic;

public class UIWindowController
{
    private List<UIWindow> windows;

    public UIWindowController(List<UIWindow> windows)
    {
        this.windows = windows;
    }

    public void Show(UIWindowId windowId)
    {
        foreach (var window in windows)
        {
            if (window.WindowId == windowId)
            {
                window.Show(instantly: false);
            }
            else
            {
                window.Hide(instantly: !window.IsShown);
            }
        }
    }
}
