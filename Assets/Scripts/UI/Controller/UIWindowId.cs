﻿public enum UIWindowId
{
    TutorialWindow,
    StartWindow,
    RunningWindow,
    VictoryWindow,
    DefeatWindow,
    UnlockWindow
}
