﻿using UnityEngine;

public class UIWindow : MonoBehaviour
{
    [SerializeField] private UIWindowId windowId;
    [SerializeField] private CanvasGroup activeGroup;
    [SerializeField] private Animation activeAnimation;

    public const int AwaitTimeMs = 3000;

    public UIWindowId WindowId => windowId;
    public bool IsShown { get; private set; }

    private const string FadeInKey = "fade_in_anim";
    private const string FadeOutKey = "fade_out_anim";
    
    public void Show(bool instantly)
    {
        gameObject.SetActive(true);

        if (!instantly)
        {
            activeAnimation.Play(FadeInKey);
        }

        activeGroup.blocksRaycasts = true;

        IsShown = true;
    }

    public void Hide(bool instantly)
    {
        activeGroup.blocksRaycasts = false;

        if (!instantly)
        {
            activeAnimation.Play(FadeOutKey);
        }

        gameObject.SetActive(false);

        IsShown = false;
    }
}
