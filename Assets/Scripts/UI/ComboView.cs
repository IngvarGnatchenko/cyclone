﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ComboView : MonoBehaviour
{
    [SerializeField] private List<Animation> comboAnimations;
    [SerializeField] private TextMeshProUGUI comboText;

    private EnvironmentModel model;

    private Dictionary<int, List<string>> goodiesMap = new Dictionary<int, List<string>>()
    {
        [2] = new List<string>()
        {
            "Nice!",
            "Yeah!",
            "Good!"
        },
        [3] = new List<string>()
        {
            "Excellent!",
            "Splendid!",
            "Brilliant!"
        },
        [4] = new List<string>()
        {
            "Godlike",
            "Unbelievable!",
            "Supreme"
        }
    };

    public void Init(EnvironmentModel model)
    {
        this.model = model;

        model.ComboMade += OnComboMade;

        comboAnimations.ForEach(item => item.gameObject.SetActive(false));
    }

    public void Deinit()
    {
        model.ComboMade -= OnComboMade;

        comboAnimations.ForEach(item => { if (item != null) item.gameObject.SetActive(false); });
    }

    private void OnComboMade(int count)
    {
        while (!goodiesMap.ContainsKey(count) && count > 1)
        {
            count--;
        }

        if (goodiesMap.TryGetValue(count, out var usedList))
        {
            comboAnimations.ForEach(item => item.gameObject.SetActive(true));

            var goodieIndex = UnityEngine.Random.Range(0, usedList.Count);

            var goodieMessage = usedList[goodieIndex];

            comboText.text = goodieMessage;

            foreach (var comboAnimation in comboAnimations)
            {
                comboAnimation.Stop();
                comboAnimation.Play();
            }
        }
    }
}
