﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DefeatGameScreen : UIWindow
{
    [SerializeField] private Button activeButton;
    [SerializeField] private TextMeshProUGUI failedText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private AudioSource defeatSound;
    [SerializeField] private AudioSource crashSound;

    private const string FaileFormat = "LEVEL {0}\nfailed!";
    private const string ScoreFormat = "Your score is\n<size=200%>{0}</size>";

    private TaskCompletionSource<object> tcs;

    public void Init()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    public void Deinit()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    public Task Wait(int levelIndex, int score)
    {
        tcs = new TaskCompletionSource<object>();

        failedText.text = string.Format(FaileFormat, levelIndex + 1);
        scoreText.text = string.Format(ScoreFormat, score);

        defeatSound.Play();
        crashSound.Play();

        return tcs.Task;
    }

    private void ActiveButtonHandler()
    {
        tcs.SetResult(null);
    }
}
