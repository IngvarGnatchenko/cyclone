﻿using System.Collections.Generic;
using UnityEngine;

public abstract class UIFloodBase<V, M> : MonoBehaviour where V : UIFloodCoinBase<M>
{
	[SerializeField] private Transform effectorPoolAnchor;
	[SerializeField] private Transform effectorActiveAnchor;

	private GameObjectPool<UIFloodCoinBase<M>> pool;

	private List<UIFloodCoinBase<M>> effectors = new List<UIFloodCoinBase<M>>();
	private List<UIFloodCoinBase<M>> effectorsToRemove = new List<UIFloodCoinBase<M>>();

	public void Init()
	{
		pool = new GameObjectPool<UIFloodCoinBase<M>>(GetViewPrefab(), effectorPoolAnchor);
	}

	public void Deinit()
	{
		foreach (var effector in effectors)
		{
			OnEffected(effector);
		}

		FlushRemovedEffectors();

		pool.Dispose();
	}

	public void UpdateEffectors()
	{
		foreach (var effector in effectors)
		{
			effector.UpdateEffector();
		}

		FlushRemovedEffectors();
	}

	public void Effect(M model, Vector3 startPosition, Vector3 endPosition, float effectTime)
	{
		var direction = endPosition - startPosition;
		var angle = Vector3.SignedAngle(Vector3.up, direction.normalized, Vector3.forward);

		var effectorView = pool.GetInstance();

		effectors.Add(effectorView);

		effectorView.transform.SetParent(effectorActiveAnchor);
		effectorView.transform.localPosition = startPosition;
		effectorView.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, angle);

		effectorView.Show();

		effectorView.Effected += OnEffected;

		effectorView.Refresh(model, direction.magnitude, effectTime);
	}

	protected abstract V GetViewPrefab();

	private void FlushRemovedEffectors()
	{
		foreach (var effector in effectorsToRemove)
		{
			effector.Effected -= OnEffected;

			effectors.Remove(effector);

			effector.Hide();

			pool.RecycleInstance(effector);
		}

		effectorsToRemove.Clear();
	}

	private void OnEffected(UIFloodCoinBase<M> effector)
	{
		effectorsToRemove.Add(effector);
	}
}