﻿using System;
using UnityEngine;

public class UIFloodCoinBase<M> : MonoBehaviour
{
	[SerializeField] private AnimationCurve forwardCurve;
	[SerializeField] private AnimationCurve sideCurve;
	[SerializeField] private Transform anchor;

	public event Action<UIFloodCoinBase<M>> Effected;

	private bool active;
	private float totalTime;
	private float currentTime;
	private float scale;
	private float totalDistance;

	public virtual void Refresh(M model, float distance, float effectTime)
	{
		active = true;

		totalTime = effectTime;
		currentTime = 0.0f;
		scale = UnityEngine.Random.Range(-200.0f, 200.0f);
		totalDistance = distance;
	}

	public void UpdateEffector()
	{
		if (active)
		{
			currentTime += Time.deltaTime;

			var ratio = currentTime / totalTime;

			UpdatePosition(ratio);

			if (ratio >= 1.0f)
			{
				active = false;

				Effected?.Invoke(this);
			}
		}
	}

	public void Show()
	{
		anchor.gameObject.SetActive(true);
	}

	public void Hide()
	{
        if (anchor != null)
        {
            anchor.gameObject.SetActive(false);
        }
	}

	private void UpdatePosition(float ratio)
	{
		var forwardRatio = forwardCurve.Evaluate(ratio);
		var sideRatio = sideCurve.Evaluate(ratio);

		var yPart = forwardRatio * totalDistance;
		var xPart = sideRatio * scale;

		anchor.localPosition = new Vector3(xPart, yPart, 0.0f);
	}
}
