﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonIndicatorView : MonoBehaviour
{
    [SerializeField] private List<Image> indicatorSteps;
    
    public void Refresh(float ratio)
    {
        for (var i = 0; i < indicatorSteps.Count; i++)
        {
            var activationRatio = (float)i / (float)indicatorSteps.Count;

            var step = indicatorSteps[i];

            step.enabled = activationRatio < ratio;
        }
    }
}
