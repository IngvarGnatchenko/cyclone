﻿using UnityEngine;

public class UIFloodFuelsView : UIFloodBase<UIFloodFuelView, FuelModel>
{
	[SerializeField] private UIFloodFuelView viewPrefab;

	protected override UIFloodFuelView GetViewPrefab()
	{
		return viewPrefab;
	}
}