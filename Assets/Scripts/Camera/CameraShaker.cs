﻿using UnityEngine;

public class CameraShaker : MonoBehaviour
{
	[SerializeField] private Transform anchor;
	[SerializeField] private AnimationCurve shakeCurve;

	private const float ShakeAmplitude = 0.5f;
	private const float ShakeFrequency = 5f;
	private const float ShakeTime = 0.5f;

	private Vector3 initialPosition;
	private float randomRow;
	private float currentTime;
	private bool isActive;

	public void Shake()
	{
		if (!isActive)
		{
			initialPosition = anchor.localPosition;
		}

		currentTime = 0.0f;

		randomRow = Random.Range(0.0f, 1.0f);

		isActive = true;
	}

	public void ExternalUpdate()
	{
		if (!isActive)
		{
			return;
		}

        currentTime += Time.deltaTime;

		var ratio = currentTime / ShakeTime;

		var shakeDecay = shakeCurve.Evaluate(ratio);

		float sampleX = shakeDecay * ShakeAmplitude * (Mathf.PerlinNoise(ratio * ShakeFrequency, randomRow) - 0.5f);
		float sampleY = shakeDecay * ShakeAmplitude * (Mathf.PerlinNoise(ratio * ShakeFrequency, 1.0f - randomRow) - 0.5f);

		var shift = new Vector3(sampleX, sampleY, 0.0f);

		anchor.localPosition = initialPosition + shift;

		if (ratio >= 1.0f)
		{
			anchor.localPosition = initialPosition;
			isActive = false;
		}
	}
}
