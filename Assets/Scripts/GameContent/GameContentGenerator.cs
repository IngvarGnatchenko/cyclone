﻿using System.Collections.Generic;
using UnityEngine;

public class GameContentGenerator
{
    private GameContent gameContent;
    private System.Random random;

    public GameContentGenerator(GameContent gameContent)
    {
        this.gameContent = gameContent;
    }

    public LevelContent GenerateLevel(int levelIndex)
    {
        random = new System.Random(levelIndex);

        var lastLevelContentId = gameContent.LevelContents.Count - 1;

        var referenceLevelContentId = lastLevelContentId - (levelIndex % 4);

        var referenceLevelContent = gameContent.LevelContents[referenceLevelContentId];

        var freePlaces = GetFreePlaces();

        return new LevelContent()
        {
            shipId = referenceLevelContent.shipId,
            IsShipUnlocked = false,
            Background = referenceLevelContent.Background,
            FuelContents = GenerateFuels(freePlaces, random.Next(8, 15)),
            WallContents = GenerateWalls(freePlaces, random.Next(3, 7)),
            CannonContents = GenerateCannons(freePlaces, random.Next(1, 4)),
            MineContents = GenerateMines(freePlaces, random.Next(10) > 6 ? random.Next(1, 4) : 0),
            HoleContents = GenerateHoles(freePlaces, random.Next(10) > 7 ? random.Next(1, 3) : 0),
        };
    }

    private List<FuelContent> GenerateFuels(List<Vector2> freePlaces, int count)
    {
        var items = new List<FuelContent>();

        var allocatedPlaces = FilterAllocatedPlaces(freePlaces, count);

        foreach (var place in allocatedPlaces)
        {
            items.Add(new FuelContent()
            {
                Type = random.Next(10) > 8 ? random.Next(11) > 5 ? FuelType.Boost : FuelType.Armor : FuelType.Normal,
                Position = place
            });
        }

        return items;
    }

    private List<WallContent> GenerateWalls(List<Vector2> freePlaces, int count)
    {
        var items = new List<WallContent>();

        var allocatedPlaces = FilterAllocatedPlaces(freePlaces, count);

        foreach (var place in allocatedPlaces)
        {
            items.Add(new WallContent()
            {
                Type = random.Next(10) > 7 ? WallType.Cicular : WallType.Static,
                Position = place,
                Amplitude = random.Next(2, 5)
            });
        }

        return items;
    }

    private List<CannonContent> GenerateCannons(List<Vector2> freePlaces, int count)
    {
        var items = new List<CannonContent>();

        var allocatedPlaces = FilterAllocatedPlaces(freePlaces, count);

        foreach (var place in allocatedPlaces)
        {
            var firePeriod = random.Next(6, 12);
            var fireShift = random.Next(Mathf.RoundToInt(firePeriod / 2.0f), firePeriod);

            items.Add(new CannonContent()
            {
                Position = place,
                FirePeriod = firePeriod,
                FireShift = fireShift
            });
        }

        return items;
    }

    private List<MineContent> GenerateMines(List<Vector2> freePlaces, int count)
    {
        var items = new List<MineContent>();

        var allocatedPlaces = FilterAllocatedPlaces(freePlaces, count);

        foreach (var place in allocatedPlaces)
        {
            items.Add(new MineContent()
            {
                Position = place,
                ActivationRadius = random.Next(1, 3),
                ShellCount = random.Next(3, 6),
                ShellSpeed = 4f
            });
        }

        return items;
    }

    private List<HoleContent> GenerateHoles(List<Vector2> freePlaces, int count)
    {
        var items = new List<HoleContent>();

        var allocatedPlaces = FilterAllocatedPlaces(freePlaces, count);

        foreach (var place in allocatedPlaces)
        {
            items.Add(new HoleContent()
            {
                Position = place,
                CaptureForce = 0.7f,
                CaptureRadius = random.Next(1, 4)
            });
        }

        return items;
    }

    private List<Vector2> GetFreePlaces()
    {
        var list = new List<Vector2>();

        for (var i = -3; i <= 3; i++)
        {
            for (var j = -4; j <= 4; j++)
            {
                var vector = new Vector2(i, j) * 2.0f + Vector2.up * 2.0f;

                list.Add(vector);
            }
        }

        return list;
    }

    private List<Vector2> FilterAllocatedPlaces(List<Vector2> freePlaces, int count)
    {
        var allocatedPlaces = new List<Vector2>();

        while (freePlaces.Count > 0 && allocatedPlaces.Count < count)
        {
            var randomIndex = random.Next(0, freePlaces.Count);

            var randomPlace = freePlaces[randomIndex];

            freePlaces.RemoveAt(randomIndex);

            allocatedPlaces.Add(randomPlace);
        }

        return allocatedPlaces;
    }
}