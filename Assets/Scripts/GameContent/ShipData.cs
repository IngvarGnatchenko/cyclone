﻿using System;
using UnityEngine;

[Serializable]
public class ShipData
{
    public int id;
    public Sprite icon;
    public string name;
    public GameObject model;
}