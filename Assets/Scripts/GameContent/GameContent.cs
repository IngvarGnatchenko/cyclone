﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameContent", menuName = "ScriptableObjects/GameContent", order = 1)]
public class GameContent : ScriptableObject
{
    public static GameContent Instance { get; set; }

    public ShipContent shipContent;

    public float PositionSpeed;
    public float RotationSpeed;
    public int NodePeriodMs;
    public int InitialNodesCount;

    public float MissileRotationSpeed;
    public float MissileSpeed;
    public float MissileDetonationDistance;
    public float MissileTimeToLive;

    public int MineActivationDelayMs;
    public float ShellDetonationDistance;
    public float ShellTimeToLive;
    public float ShellRotationSpeed;

    public float BoostTime;
    public float ArmorTime;
    public float BoostFactor;

    public AnimationCurve WallMoveCurve;

    public List<LevelContent> LevelContents;

    private GameContentGenerator contentGenerator;

    public LevelContent GetLevelContentByIndex(int levelIndex)
    {
        if (contentGenerator == null)
        {
            contentGenerator = new GameContentGenerator(this);
        }

        if (levelIndex < LevelContents.Count)
        {
            return LevelContents[levelIndex];
        }

        return contentGenerator.GenerateLevel(levelIndex);
    }
}

[Serializable]
public class LevelContent
{
    public int shipId;
    public bool IsShipUnlocked;
    public Sprite Background;
    public List<FuelContent> FuelContents;
    public List<CannonContent> CannonContents;
    public List<HoleContent> HoleContents;
    public List<MineContent> MineContents;
    public List<WallContent> WallContents;

    public ShipData ShipContent => GameContent.Instance.shipContent.data.Find(item => item.id == shipId);
}

[Serializable]
public class FuelContent
{
    public FuelType Type;
    public Vector2 Position;
    public int Score = 5;
}

[Serializable]
public class CannonContent
{
    public Vector2 Position;
    public Vector2 Direction;
    public float FireShift = 6;
    public float FirePeriod = 6;
}

[Serializable]
public class WallContent
{
    public WallType Type;
    public Vector2 Position;
    public int Direction = 1;
    public float Amplitude = 5;
    public float TotalTime = 5;
    public float TimeShift = 0;
}

[Serializable]
public enum WallType
{
    Static,
    HorizontalDynamic,
    VerticalDynamic,
    Cicular
}

[Serializable]
public enum FuelType
{
    Normal,
    Boost,
    Armor
}

[Serializable]
public class HoleContent
{
    public Vector2 Position;
    public float CaptureRadius = 1f;
    public float CaptureForce = 0.5f;
}

[Serializable]
public class MineContent
{
    public Vector2 Position;
    public float ActivationRadius = 1.0f;
    public int ShellCount = 4;
    public float ShellSpeed = 1.0f;
}
