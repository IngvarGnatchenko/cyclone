﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipContent", menuName = "ScriptableObjects/ShipContent", order = 1)]
public class ShipContent : ScriptableObject
{
    public List<ShipData> data = new List<ShipData>();
}