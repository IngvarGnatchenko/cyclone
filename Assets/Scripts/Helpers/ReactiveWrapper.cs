﻿using System;

public class ReactiveWrapper<T>
{
    public event Action<T> Changed;
    public event Action<T, T> Difference;

    public T Value
    {
        get
        {
            return internalValue;
        }

        set
        {
            var prevValue = internalValue;

            internalValue = value;

            Changed?.Invoke(internalValue);
            Difference?.Invoke(prevValue, internalValue);
        }
    }

    private T internalValue;
}
