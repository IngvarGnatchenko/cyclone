﻿using System;

[Serializable]
public class PointerArrowSpeedParameters
{
    public int minDistance;
    public int maxDistance;
    public float minSpeedFactor;
    public float maxSpeedFactor;
}
