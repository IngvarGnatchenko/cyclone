﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PointerJoystick : MonoBehaviour
{
    [SerializeField] private RectTransform staticPart;
    [SerializeField] private RectTransform dynamicPart;
    [SerializeField] private PointerRegistrator pointerRegistrator;
    [SerializeField] private CanvasScaler canvasScaler;

    public event Action<Vector2> DirectionChanged;

    private Vector2 startPosition;
    
    public void Init()
    {
        pointerRegistrator.Down += OnDown;
        pointerRegistrator.Up += OnUp;
        pointerRegistrator.Drag += OnDrag;

        UpdateVisual(startPosition, Vector2.zero, 0.0f, false);
    }

    public void Deinit()
    {
        pointerRegistrator.Down -= OnDown;
        pointerRegistrator.Up -= OnUp;
        pointerRegistrator.Drag -= OnDrag;
    }

    private void OnDown(PointerEventData eventData)
    {
        startPosition = eventData.position;

        UpdateVisual(startPosition, Vector2.zero, 0.0f, true);
    }

    private void OnUp(PointerEventData eventData)
    {
        UpdateVisual(startPosition, Vector2.zero, 0.0f, false);
    }

    private void OnDrag(PointerEventData eventData)
    {
        var currentPosition = eventData.position;

        var targetVector = currentPosition - startPosition;
        var targetDirection = targetVector.normalized;
        var targetDistance = targetVector.magnitude;

        var handleLength = staticPart.rect.width / 2.0f * canvasScaler.transform.localScale.x;

        if (Vector3.Distance(startPosition, currentPosition) > handleLength)
        {
            startPosition = currentPosition - targetDirection * handleLength;
        }

        UpdateVisual(startPosition, targetDirection, Mathf.Min(targetDistance, handleLength), true);

        DirectionChanged?.Invoke(targetDirection);
    }

    private void UpdateVisual(Vector2 startPosition, Vector2 direction, float length, bool status)
    {
        staticPart.position = startPosition;
        dynamicPart.position = startPosition + direction * length;

        staticPart.gameObject.SetActive(status);
        dynamicPart.gameObject.SetActive(status);
    }
}
