﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerRegistrator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public event Action<PointerEventData> Down;
    public event Action<PointerEventData> Up;
    public event Action<PointerEventData> Drag;

    public void OnPointerDown(PointerEventData eventData)
    {
        Down?.Invoke(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Up?.Invoke(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Drag?.Invoke(eventData);
    }
}
