﻿using System;
using UnityEngine;

public class CycledTimer
{
    public event Action Ticked;

    private int initialPeriodMs;
    private int runningPeriodMs;

    private bool isRunning;

    public void Fire(int periodMs)
    {
        initialPeriodMs = periodMs;
        runningPeriodMs = periodMs;

        isRunning = true;
    }

    public void Update()
    {
        if (!isRunning)
        {
            return;
        }

        runningPeriodMs = runningPeriodMs - (int)(Time.deltaTime * 1000);

        if (runningPeriodMs < 0)
        {
            runningPeriodMs = initialPeriodMs;

            Ticked?.Invoke();
        }
    }

    public void Stop()
    {
        isRunning = false;
    }
}
