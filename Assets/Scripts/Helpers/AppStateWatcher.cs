﻿using System;
using UnityEngine;

public class AppStateWatcher : MonoBehaviour
{
    public event Action<bool> ApplicationPause;

#if !UNITY_EDITOR
	private void OnApplicationPause(bool pause)
	{
		ApplicationPause?.Invoke(pause);
	}
#endif

#if UNITY_EDITOR
    private void OnApplicationFocus(bool pause)
    {
        ApplicationPause?.Invoke(!pause);
    }
#endif
}
