﻿using System;
using UnityEngine;

public class ValueMorpher
{
	public event Action<long> ValueChanged;

	private const int StepsCount = 10;
	private const int TotalDurationMs = 250;

	private const int StepDurationMs = TotalDurationMs / StepsCount;

	private long targetValue;
	private long runningValue;

	private int runningIndex;
	private int runningDuration;

	private bool isRunning;

	public void Start(long value)
	{
		runningIndex = 0;
		runningDuration = 0;

		runningValue = targetValue;
		targetValue = value;

		isRunning = true;
	}

	public void Stop()
	{
		isRunning = false;
	}

	public void Update()
	{
		if (!isRunning)
		{
			return;
		}

		runningDuration -= (int)(Time.deltaTime * 1000);

		if (runningDuration < 0)
		{
			var ratio = (float)(runningIndex + 1) / (float)StepsCount;

			runningIndex += 1;
			runningDuration = StepDurationMs;

			ValueChanged?.Invoke((long)(Mathf.Lerp(runningValue, targetValue, ratio)));
		}

		if (runningIndex == StepsCount)
		{
			Stop();
		}
	}
}
