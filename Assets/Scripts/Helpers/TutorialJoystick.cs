﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TutorialJoystick : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;

    public event Action<Vector2> DirectionChanged;

    private const float CheckWaypointDistance = 0.25f;

    private ShipModel ship;

    private int runningIndex;

    public void Init(ShipModel ship)
    {
        this.ship = ship;
    }

    public void Deinit()
    {
    }

    public void ExternalUpdate()
    {
        UpdateWaypoint();
        UpdateDirection();
    }

    private void UpdateWaypoint()
    {
        var shipPosition = ship.Position.Value;

        var waypoint = waypoints[runningIndex];
        var waypointPosition = new Vector2(waypoint.localPosition.x, waypoint.localPosition.y);

        if (CheckWaypoint(shipPosition, waypointPosition))
        {
            runningIndex = (runningIndex + 1) % waypoints.Count;
        }
    }

    private void UpdateDirection()
    {
        var shipPosition = ship.Position.Value;

        var waypoint = waypoints[runningIndex];
        var waypointPosition = new Vector2(waypoint.localPosition.x, waypoint.localPosition.y);

        var direction = (waypointPosition - shipPosition).normalized;

        DirectionChanged?.Invoke(direction);
    }

    private bool CheckWaypoint(Vector2 shipPosition, Vector2 waypointPosition)
    {
        return Vector2.Distance(shipPosition, waypointPosition) < CheckWaypointDistance;
    }

}
