﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PointerArrow : MonoBehaviour
{
    [SerializeField] private LineRenderer targetLine;
    [SerializeField] private RectTransform dynamicPart;
    [SerializeField] private PointerRegistrator pointerRegistrator;
    [SerializeField] private CanvasScaler canvasScaler;
    [SerializeField] private Transform activeAnchor;

    private const float InchesUpTarget = 1.5f;
    private const float InchesForBorder = 0.25f;

    public event Action<Vector2> TargetChanged;
    public event Action<float> SpeedFactorChanged;

    private Camera activeCamera;
    private ShipModel ship;
    private Vector2 targetScreenPosition;
    private Vector2 relativeWorldPosition;
    private bool firstHandled;

    private PointerArrowSpeedParameters parameters;

    public void Init(Camera activeCamera, ShipModel ship, PointerArrowSpeedParameters parameters)
    {
        firstHandled = false;

        this.activeCamera = activeCamera;
        this.ship = ship;
        this.parameters = parameters;

        pointerRegistrator.Down += OnDown;
        pointerRegistrator.Drag += OnDrag;

        DrawTargetLine(firstHandled);
    }

    public void Deinit()
    {
        firstHandled = false;

        pointerRegistrator.Down -= OnDown;
        pointerRegistrator.Drag -= OnDrag;

        DrawTargetLine(firstHandled);
    }

    public void ExternalUpdate()
    {
        DrawTargetLine(firstHandled);

        HandleSpeedFactor(relativeWorldPosition);
    }

    private void HandleSpeedFactor(Vector2 targetPosition)
    {
        var speedFactor = GetSpeedFactor(ship.Position.Value, targetPosition);

        SpeedFactorChanged?.Invoke(speedFactor);
    }

    private void OnDown(PointerEventData eventData)
    {
        HandlePoint(eventData.position);
    }

    private void OnDrag(PointerEventData eventData)
    {
        HandlePoint(eventData.position);
    }

    private void HandlePoint(Vector2 currentPosition)
    {
        firstHandled = true;

        targetScreenPosition = currentPosition;
        targetScreenPosition = targetScreenPosition + InchesUpTarget * Vector2.up * Screen.dpi * canvasScaler.transform.localScale.x;
        targetScreenPosition = ClampByScreen(targetScreenPosition);
        var targetWorldPosition = activeCamera.ScreenToWorldPoint(targetScreenPosition);
        relativeWorldPosition = activeAnchor.InverseTransformPoint(targetWorldPosition);

        TargetChanged?.Invoke(relativeWorldPosition);
    }

    private float GetSpeedFactor(Vector2 shipPosition, Vector2 targetPosition)
    {
        var distance = Vector2.Distance(shipPosition, targetPosition);

        var ratio = (float)(distance - parameters.minDistance) / (float)(parameters.maxDistance - parameters.minDistance);

        var speedFactor = Mathf.Lerp(parameters.minSpeedFactor, parameters.maxSpeedFactor, ratio);

        return speedFactor;
    }

    private void DrawTargetLine(bool state)
    {
        if (state)
        {
            dynamicPart.position = targetScreenPosition;
            dynamicPart.gameObject.SetActive(true);
            targetLine.positionCount = 2;
            targetLine.SetPosition(0, activeAnchor.TransformPoint(ship.Position.Value));
            targetLine.SetPosition(1, activeAnchor.TransformPoint(relativeWorldPosition));
        }
        else
        {
            targetScreenPosition = Vector2.zero;
            relativeWorldPosition = Vector2.zero;
            if (dynamicPart != null) dynamicPart.gameObject.SetActive(false);
            if (targetLine != null) targetLine.positionCount = 0;
        }
    }

    private Vector2 ClampByScreen(Vector2 inputPosition)
    {
        var inchesInPixels = InchesForBorder * Screen.dpi * canvasScaler.transform.localScale.x;

        var x = Mathf.Clamp(inputPosition.x, inchesInPixels, Screen.width - inchesInPixels);
        var y = Mathf.Clamp(inputPosition.y, inchesInPixels, Screen.height - inchesInPixels);

        return new Vector2(x, y);
    }
}
