﻿using UnityEngine;

public class UISpriteFitter : MonoBehaviour
{
	[SerializeField] private Camera backcamera;
	[SerializeField] private Transform background;
	[SerializeField] private float backgroundMultiplier = 1.0f;

	private const float PixelsPerUnit = 128;
	private const float ReferenceWidth = 1080;
	private const float ReferenceHeight = 1920;

	private void Awake()
	{
		RefreshBackground();
	}

	private void RefreshBackground()
	{
		var screenRatio = (float)Screen.width / (float)Screen.height;
		var cameraHeightInUnits = backcamera.orthographicSize * 2;
		var cameraWidthInUnits = screenRatio * cameraHeightInUnits;

		var backgroundHeightInUnits = ReferenceHeight / PixelsPerUnit;
		var backgroundWidthInUnits = ReferenceWidth / PixelsPerUnit;

		var screenType = UIUtils.GetScreenType();

		var scaleRatio = screenType == UIUtils.ScreenType.Wide ?
			cameraWidthInUnits / backgroundWidthInUnits :
			cameraHeightInUnits / backgroundHeightInUnits;

		background.localScale = Vector3.one * scaleRatio * backgroundMultiplier;

		// mew
		foreach (Transform child in background)
		{
			child.localScale = Vector3.one;
		}
	}
}