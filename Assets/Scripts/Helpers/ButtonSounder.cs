﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonSounder : MonoBehaviour
{
    [SerializeField] private Button activeButton;
    [SerializeField] private AudioSource activeSound;

    private void Awake()
    {
        activeButton.onClick.AddListener(ActiveButtonHandler);
    }

    private void OnDestroy()
    {
        activeButton.onClick.RemoveListener(ActiveButtonHandler);
    }

    private void ActiveButtonHandler()
    {
        if (activeSound != null)
        {
            activeSound.Play();
        }
    }
}