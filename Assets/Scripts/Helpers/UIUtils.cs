﻿using UnityEngine;

public static class UIUtils
{
	private const float OriginalRatio = 9f / 16f;

	public static ScreenType GetScreenType()
	{
		var screenWidth = Screen.width;
		var screenHeight = Screen.height;

		var ratio = (float)screenWidth / (float)screenHeight;

		return ratio > OriginalRatio ? ScreenType.Wide : ScreenType.Original;
	}

	public static void ToggleGroup(this CanvasGroup group, bool isActive)
	{
		group.alpha = isActive ? 1.0f : 0.0f;
		group.blocksRaycasts = isActive;
	}

    public static Quaternion GetQuaternionFromVector(Vector2 direction)
    {
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public static Vector2 GetVectorFromQuaternion(Quaternion direction)
    {
        return direction * Vector3.right;
    }

    public static Vector2 FromWorldToCanvasLocal(RectTransform canvas, Camera camera, Vector3 position)
    {
        var offset = new Vector2(canvas.sizeDelta.x / 2f, canvas.sizeDelta.y / 2f);
        var viewport = camera.WorldToViewportPoint(position);
        var proportional = new Vector2(viewport.x * canvas.sizeDelta.x, viewport.y * canvas.sizeDelta.y);

        return proportional - offset;
    }

    public static Rect GetScreenBorder(Camera activeCamera)
    {
        var screenRatio = (float)Screen.width / (float)Screen.height;
        var cameraHeightInUnits = activeCamera.orthographicSize * 2;
        var cameraWidthInUnits = screenRatio * cameraHeightInUnits;

        var sourcePoint = activeCamera.transform.localPosition;

        return new Rect(sourcePoint.x - cameraWidthInUnits / 2.0f, sourcePoint.y - cameraHeightInUnits / 2.0f, cameraWidthInUnits, cameraHeightInUnits);
    }

    public static void Quit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public enum ScreenType
	{
		Original,
		Wide
	}
}
