﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenFitter : MonoBehaviour
{
	[SerializeField] private CanvasScaler canvasScaler;
	[SerializeField] private RectTransform notchTransform;
	[SerializeField] private bool auto;

	private void Start()
	{
		if (auto)
		{
			Fit();
		}
	}

	public void Fit()
	{
		InternalFit();

		if (notchTransform != null)
		{
			InternalNotch();
		}
	}

	private void InternalFit()
	{
		var screenType = UIUtils.GetScreenType();

		canvasScaler.matchWidthOrHeight = screenType == UIUtils.ScreenType.Original ? 0.0f : 1.0f;
	}

	private void InternalNotch()
	{
		var safeRect = Screen.safeArea;

		Vector2 anchorMin = safeRect.position;
		Vector2 anchorMax = safeRect.position + safeRect.size;

		anchorMin.x /= Screen.width;
		anchorMin.y /= Screen.height;
		anchorMax.x /= Screen.width;
		anchorMax.y /= Screen.height;

		notchTransform.anchorMin = anchorMin;
		notchTransform.anchorMax = anchorMax;
	}
}
