﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectPool<T> : IDisposable where T : MonoBehaviour
{
    private T prefab;
    private Transform anchor;
    private readonly Stack<T> itemsCache = new Stack<T>();

    public ObjectPool(T prefab, Transform anchor)
    {
        this.prefab = prefab;
        this.anchor = anchor;
    }

    public T GetInstance()
    {
        var item = itemsCache.Count > 0 ? itemsCache.Pop() : GameObject.Instantiate(prefab, anchor);

        Activate(item);

        return item;
    }

    public void RecycleInstance(T item)
    {
        Deactivate(item);

        itemsCache.Push(item);
    }

    public void Dispose()
    {
        foreach (var item in itemsCache)
        {
            if (item != null && item.gameObject != null)
            {
                GameObject.Destroy(item.gameObject);
            }
        }
    }

    protected abstract void Activate(T instance);
    protected abstract void Deactivate(T instance);
}

public class GameObjectPool<T> : ObjectPool<T> where T : MonoBehaviour
{
    public GameObjectPool(T prefab, Transform anchor) : base(prefab, anchor) { }

    protected override void Activate(T instance)
    {
        instance.gameObject.SetActive(true);
    }

    protected override void Deactivate(T instance)
    {
        instance.gameObject.SetActive(false);
    }
}