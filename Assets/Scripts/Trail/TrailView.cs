﻿using System.Collections.Generic;
using UnityEngine;

public class TrailView : MonoBehaviour
{
    [SerializeField] private NodeView viewPrefab;
    [SerializeField] private Transform viewAnchor;
    [SerializeField] private Transform activeAnchor;
    [SerializeField] private LineRenderer lineRenderer;

    private List<NodeView> views = new List<NodeView>();

    private TrailModel model;

    public void Init(TrailModel model)
    {
        this.model = model;

        model.HeadNode.Changed += OnHeadChanged;
        model.TailNode.Difference += OnTailChanged;
    }

    public void Deinit()
    {
        model.HeadNode.Changed -= OnHeadChanged;
        model.TailNode.Difference -= OnTailChanged;

        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    private void OnHeadChanged(NodeModel node)
    {
        var view = GetView();

        view.Init(node);

        views.Add(view);

        RedrawTrail();
    }

    private void OnTailChanged(NodeModel prevNode, NodeModel newNode)
    {
        if (prevNode == null)
        {
            return;
        }

        var prevView = views.Find(view => view.Model.Id == prevNode.Id);

        views.Remove(prevView);

        prevView.Deinit();

        DestroyView(prevView);

        RedrawTrail();
    }

    private void RedrawTrail()
    {
        lineRenderer.positionCount = views.Count;

        for (var i = 0; i < views.Count; i++)
        {
            var view = views[i];

            lineRenderer.SetPosition(i, activeAnchor.TransformPoint(view.Model.Position));
        }
    }

    private NodeView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(NodeView view)
    {
        GameObject.Destroy(view.gameObject);
    }
}
