﻿using UnityEngine;

public class IntersectionData
{
    public NodeModel Intersector { get; }
    public NodeModel Intersectee { get; }
    public Vector2 Intersection { get; }

    public IntersectionData(NodeModel intersector, NodeModel intersectee, Vector2 intersection)
    {
        Intersector = intersector;
        Intersectee = intersectee;
        Intersection = intersection;
    }
}
