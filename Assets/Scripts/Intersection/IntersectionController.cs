﻿using System;

public class IntersectionController
{
    public event Action<IntersectionData> IntersectionFound;

    private TrailModel model;

    public IntersectionController(TrailModel model)
    {
        this.model = model;
    }

    public void Init()
    {
        model.HeadNode.Changed += OnHeadChanged;
    }

    public void Deinit()
    {
        model.HeadNode.Changed -= OnHeadChanged;
    }

    private void OnHeadChanged(NodeModel intersector)
    {
        var intersectionData = CheckIntersection(intersector);

        if (intersectionData != null)
        {
            IntersectionFound?.Invoke(intersectionData);
        }
    }

    private IntersectionData CheckIntersection(NodeModel intersector)
    {
        var runningNode = intersector.Parent;

        if (runningNode == null)
        {
            return null;
        }

        var p1 = intersector.Position;
        var p2 = runningNode.Position;

        var isInitial = true;

        while (runningNode.Parent != null)
        {
            var p3 = runningNode.Position;
            var p4 = runningNode.Parent.Position;

            var isIntersected = GeometryHelper.CheckIntersection(p1, p2, p3, p4, out var intersection);

            if (isIntersected && !isInitial)
            {
                var data = new IntersectionData(
                    intersector: intersector,
                    intersectee: runningNode,
                    intersection: intersection);

                return data;
            }
            else
            {
                runningNode = runningNode.Parent;

                isInitial = false;
            }
        }

        return null;
    }
}
