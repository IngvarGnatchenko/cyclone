﻿using System.Collections.Generic;
using UnityEngine;

public class CannonAssembler : MonoBehaviour
{
    [SerializeField] private CannonView viewPrefab;
    [SerializeField] private Transform viewAnchor;
    [SerializeField] private Transform missileViewAnchor;

    private List<CannonController> controllers = new List<CannonController>();
    private List<CannonView> views = new List<CannonView>();

    private List<CannonController> temp = new List<CannonController>();

    public void Init(List<CannonModel> cannons, ShipModel shipModel)
    {
        foreach (var cannon in cannons)
        {
            var controller = new CannonController(cannon, shipModel);
            var view = GetView();

            controller.Init();
            view.Init(cannon, missileViewAnchor);

            controllers.Add(controller);
            views.Add(view);
        }
    }

    public void Deinit()
    {
        foreach (var controller in controllers)
        {
            controller.Deinit();
        }

        controllers.Clear();

        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    public void ExternalUpdate()
    {
        temp.Clear();

        temp.AddRange(controllers);

        foreach (var controller in temp)
        {
            controller.Update();
        }

        views.ForEach(view => view.ExternalUpdate());
    }

    private CannonView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(CannonView view)
    {
        GameObject.Destroy(view.gameObject);
    }
}
