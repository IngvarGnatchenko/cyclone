﻿using System.Collections.Generic;
using UnityEngine;

public class CannonView : MonoBehaviour
{
    [SerializeField] private CannonIndicatorView indicator;
    [SerializeField] private MissileView viewPrefab;
    [SerializeField] private Transform positionAnchor;
    [SerializeField] private Transform rotationAnchor;

    private CannonModel model;

    private List<MissileView> views = new List<MissileView>();

    private Transform viewAnchor;

    public void Init(CannonModel model, Transform viewAnchor)
    {
        this.model = model;
        this.viewAnchor = viewAnchor;

        model.MissileAdded += OnMissileAdded;
        model.MissileDetonated += OnMissileDetonated;

        model.Direction.Changed += OnDirectionChanged;
        model.Position.Changed += OnPositionChanged;

        OnDirectionChanged(model.Direction.Value);
        OnPositionChanged(model.Position.Value);
    }

    public void Deinit()
    {
        model.MissileAdded -= OnMissileAdded;
        model.MissileDetonated -= OnMissileDetonated;

        model.Direction.Changed -= OnDirectionChanged;
        model.Position.Changed -= OnPositionChanged;

        DeinitMissiles();
    }

    public void ExternalUpdate()
    {
        UpdateTimer();

        views.ForEach(view => view.ExternalUpdate());
    }

    private void UpdateTimer()
    {
        indicator.Refresh(model.NormalizedLife);
    }

    private void OnMissileAdded(MissileModel missile)
    {
        var view = GetView();

        view.Init(missile);

        views.Add(view);
    }

    private void OnMissileDetonated(MissileModel missile)
    {
        var view = views.Find(item => item.Model.Id == missile.Id);

        views.Remove(view);

        view.Deinit();

        DestroyView(view);
    }

    private void OnDirectionChanged(Vector2 direction)
    {
        rotationAnchor.localRotation = UIUtils.GetQuaternionFromVector(direction);
    }

    private void OnPositionChanged(Vector2 position)
    {
        positionAnchor.localPosition = position;
    }

    private void DeinitMissiles()
    {
        foreach (var view in views)
        {
            view.Deinit();

            DestroyView(view);
        }

        views.Clear();
    }

    private MissileView GetView()
    {
        return GameObject.Instantiate(viewPrefab, viewAnchor);
    }

    private void DestroyView(MissileView view)
    {
        GameObject.Destroy(view.gameObject);
    }
}
