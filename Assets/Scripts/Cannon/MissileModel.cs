﻿using UnityEngine;

public class MissileModel
{
    public int Id { get; }
    public float TimeLeft { get; set; }
    public float NormalizedLife => 1.0f - Mathf.Clamp01(TimeLeft / timeToLive);

    public ReactiveWrapper<Vector2> Position = new ReactiveWrapper<Vector2>();
    public ReactiveWrapper<Vector2> Direction = new ReactiveWrapper<Vector2>();

    private readonly float timeToLive;

    public MissileModel(int id, Vector2 position, Vector2 direction, float timeToLive)
    {
        Id = id;
        Position.Value = position;
        Direction.Value = direction;
        TimeLeft = timeToLive;
        this.timeToLive = timeToLive;
    }
}