﻿using System.Collections.Generic;
using UnityEngine;

public class CannonController
{
    private CannonModel cannon;
    private ShipModel ship;

    private List<MissileModel> detonatedTemp = new List<MissileModel>();
    private List<MissileModel> expiredTemp = new List<MissileModel>();

    private int idCounter;

    public CannonController(CannonModel cannon, ShipModel ship)
    {
        this.cannon = cannon;
        this.ship = ship;
    }

    public void Init()
    {
        cannon.TimeLeft = cannon.Content.FireShift;
        cannon.Direction.Value = cannon.Content.Direction;
        cannon.Position.Value = cannon.Content.Position;
    }

    public void Deinit()
    {
    }

    public void Update()
    {
        ProcessMissiles();

        ProcessCannonDirection();

        ProcessFire();
    }

    private void OnTicked()
    {
        var timeToLive = GameContent.Instance.MissileTimeToLive;

        var missile = new MissileModel(idCounter++, cannon.Position.Value, cannon.Direction.Value, timeToLive);

        cannon.Missiles.Add(missile);

        cannon.PublishMissileAdded(missile);
    }

    private void ProcessCannonDirection()
    {
        var shipPosition = ship.Position.Value;
        var cannonPosition = cannon.Position.Value;

        var cannonDirection = (shipPosition - cannonPosition).normalized;

        cannon.Direction.Value = cannonDirection;
    }

    private void ProcessMissiles()
    {
        detonatedTemp.Clear();
        expiredTemp.Clear();

        foreach (var missile in cannon.Missiles)
        {
            ProcessDirection(missile, ship);
            ProcessPosition(missile);
            ProcessLife(missile);

            if (IsMissleExpired(missile))
            {
                expiredTemp.Add(missile);
            }
            else
            {
                if (IsMissileDetonated(missile, ship))
                {
                    detonatedTemp.Add(missile);
                }
            }
        }

        foreach (var missile in expiredTemp)
        {
            cannon.Missiles.Remove(missile);
            cannon.PublishMissileDetonated(missile);
        }

        foreach (var missile in detonatedTemp)
        {
            cannon.Missiles.Remove(missile);
            cannon.PublishMissileDetonated(missile);
        }

        if (detonatedTemp.Count > 0)
        {
            if (!ship.Armor.Value)
            {
                ship.PublishDetonated();
            }
        }
    }

    private void ProcessFire()
    {
        cannon.TimeLeft -= Time.deltaTime;

        if (cannon.NormalizedLife >= 1.0f)
        {
            OnTicked();

            cannon.TimeLeft = cannon.Content.FirePeriod;
        }
    }

    private bool IsMissileDetonated(MissileModel missile, ShipModel ship)
    {
        var distance = Vector2.Distance(missile.Position.Value, ship.Position.Value);

        return distance <= GameContent.Instance.MissileDetonationDistance;
    }

    private bool IsMissleExpired(MissileModel missile)
    {
        return missile.NormalizedLife >= 1.0f;
    }

    private void ProcessDirection(MissileModel missile, ShipModel ship)
    {
        var shipPosition = ship.Position.Value;
        var missilePosition = missile.Position.Value;

        var targetDirection = (shipPosition - missilePosition).normalized;

        missile.Direction.Value = Vector3.RotateTowards(
            missile.Direction.Value, 
            targetDirection, 
            GameContent.Instance.MissileRotationSpeed * Time.deltaTime, 
            0.0f);
    }

    private void ProcessPosition(MissileModel missile)
    {
        missile.Position.Value += 
            missile.Direction.Value * GameContent.Instance.MissileSpeed * Time.deltaTime;
    }

    private void ProcessLife(MissileModel missile)
    {
        missile.TimeLeft -= Time.deltaTime;
    }
}
