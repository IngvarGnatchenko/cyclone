﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CannonModel
{
    public event Action<MissileModel> MissileAdded;
    public event Action<MissileModel> MissileDetonated;

    public ReactiveWrapper<Vector2> Direction = new ReactiveWrapper<Vector2>();
    public ReactiveWrapper<Vector2> Position = new ReactiveWrapper<Vector2>();

    public CannonContent Content { get; }
    public float TimeLeft { get; set; }
    public float NormalizedLife => 1.0f - Mathf.Clamp01(TimeLeft / Content.FirePeriod);

    public List<MissileModel> Missiles = new List<MissileModel>();

    public CannonModel(CannonContent content)
    {
        Content = content;
    }

    public void PublishMissileAdded(MissileModel missile)
    {
        MissileAdded?.Invoke(missile);
    }

    public void PublishMissileDetonated(MissileModel missile)
    {
        MissileDetonated?.Invoke(missile);
    }
}
