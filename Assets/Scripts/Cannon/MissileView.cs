﻿using UnityEngine;
using UnityEngine.UI;

public class MissileView : MonoBehaviour
{
    [SerializeField] private Image timerBar;
    [SerializeField] private Transform positionAnchor;
    [SerializeField] private Transform rotationAnchor;

    public MissileModel Model { get; private set; }

    public void Init(MissileModel model)
    {
        Model = model;

        model.Position.Changed += OnPositionChanged;
        model.Direction.Changed += OnDirectionChanged;

        OnPositionChanged(model.Position.Value);
        OnDirectionChanged(model.Direction.Value);

        timerBar.fillAmount = 1.0f;
    }

    public void Deinit()
    {
        Model.Position.Changed -= OnPositionChanged;
        Model.Direction.Changed -= OnDirectionChanged;
    }

    public void ExternalUpdate()
    {
        var timerRatio = 1.0f - Model.NormalizedLife;

        timerBar.fillAmount = timerRatio;
    }

    private void OnPositionChanged(Vector2 position)
    {
        positionAnchor.localPosition = position;
    }

    private void OnDirectionChanged(Vector2 direction)
    {
        rotationAnchor.localRotation = UIUtils.GetQuaternionFromVector(direction);
    }
}
